import Test.HUnit

import Data.Logic.ATP.DefCNF (testDefCNF, Atom(N), defcnfs)
import Data.Logic.ATP.DP (
      dp,   dpsat,   dptaut
    , dpli, dplisat, dplitaut
    , dpll, dpllsat, dplltaut
    , dplb, dplbsat, dplbtaut
    , testDP )
import Data.Logic.ATP.FOL (testFOL)
import Data.Logic.ATP.Formulas (IsFormula(AtomOf, true, false, asBool, atomic))
import Data.Logic.ATP.Herbrand (testHerbrand)
import Data.Logic.ATP.Lib (testLib)
import Data.Logic.ATP.Lit ((.~.), convertLiteral, IsLiteral, JustLiteral, LFormula)
import Data.Logic.ATP.Pretty (Pretty(pPrint), prettyShow)
import Data.Logic.ATP.Prop (testProp, PFormula, simpdnf, simpcnf, (∨), (∧), (.<=>.)
  , (.&.), (.|.), BinOp(..))
import Data.Logic.ATP.PropExamples (testPropExamples)
import Data.Logic.ATP.Skolem (testSkolem)
import Data.Logic.ATP.ParserTests (testParser)
import Data.Logic.ATP.Unif (testUnif)
import Data.Logic.ATP.Tableaux (testTableaux)
import Data.Logic.ATP.Resolution (testResolution)
import Data.Logic.ATP.Prolog (testProlog)
import Data.Logic.ATP.Meson (testMeson)
import Data.Logic.ATP.Equal (testEqual)

import System.Exit (exitWith, ExitCode(ExitSuccess, ExitFailure))

import Data.Set as Set
import Data.List as List
import Data.Function (on)

strings :: Pretty a => Set (Set a) -> [[String]]
strings ss = sortBy (compare `on` length) . sort . Set.toList $ Set.map (sort . Set.toList . Set.map prettyShow) ss

testfm :: PFormula Atom
testfm = let (p, q, r, s) = (atomic (N "p" 0), atomic (N "q" 0), atomic (N "r" 0), atomic (N "s" 0)) in
     (p .|. (q .&. ((.~.) r))) .&. s

testSimpdnf :: Test
testSimpdnf =
    let input = strings (simpdnf id testfm :: Set (Set (LFormula Atom)))
        expected = [["p","s"],["q","s","¬r"]] in
    TestCase $ assertEqual "simpdnf" expected input

testSimpcnf :: Test
testSimpcnf =
    let input = strings (simpcnf id testfm :: Set (Set (LFormula Atom)))
        expected = [["s"],["p","q"],["p","¬r"]] in
    TestCase $ assertEqual "simpcnf" expected input
    
testDefcnfs :: Test
testDefcnfs =
    let input = strings (defcnfs testfm :: Set (Set (LFormula Atom)))
        expected = [["s"],["p","p_1"],["q","¬p_1"],["¬p_1","¬r"],["p_1","r","¬q"]] in
    TestCase $ assertEqual "defcnfs" expected input
    
testExtension1 :: Test
-- dpli :: (IsLiteral formula, Ord formula) => Set (formula, TrailMix) -> Set (Set formula) -> Bool
-- dplisat = dpli Set.empty . defcnfs
-- dplitaut = not . dplisat . negate
testExtension1 = TestCase $ assertEqual "valid extension" True input 
  where
    input = dpli Set.empty cnfFormat
    notp = (.~.) (atomic (N "p" 0))
    cnfFormat = defcnfs (testfm .&. notp) 
  
testExtension2 = TestCase $ assertEqual "valid extension" False input 
  where
    input = dpli Set.empty cnfFormat
    notp = (.~.) (atomic (N "p" 0))
    notq = (.~.) (atomic (N "q" 0))
    cnfFormat = defcnfs ((testfm .&. notp) .&. notq) 

main :: IO ()
main = runTestTT (TestList  [TestLabel "DefCNF" testDefCNF,
                             TestLabel "DP" testDP,
                             TestLabel "Tableaux" testTableaux,
                             TestLabel "Resolution" testResolution,
                             TestLabel "Prolog" testProlog,
                             TestLabel "Meson" testMeson,
                             TestLabel "TestATP" (TestList [testSimpdnf,
                                testSimpcnf, testDefcnfs, testExtension1,
                                testExtension2])
                             ]) >>= doCounts
    where
      doCounts counts' = exitWith (if errors counts' /= 0 || failures counts' /= 0 then ExitFailure 1 else ExitSuccess)
