import Test.HUnit
import Data.Tree
import Data.String (IsString(fromString))
import Data.Logic.ATP.FOL (EqFormula)
import Data.Logic.ATP.Pretty (Pretty(pPrint), prettyShow)
import qualified Data.Logic.ATP.Prop as Prop (
  Prop(P, pname),
  PFormula(..),
  psimplify )
import Data.Logic.ATP.DP (
  dpllsat, dpll )
import Data.Logic.ATP.Prop (testProp, PFormula(..), dnf, cnf', simpcnf, simpdnf, (∨), (∧), (.<=>.)
  , (.&.), (.|.), BinOp(..))
import qualified Data.Logic.ATP.Lit as Lit (
  LFormula(..), IsLiteral(..) )
import qualified Data.Logic.ATP.Formulas as Formulas (
  IsFormula(..) )
import qualified Data.Logic.ATP.Unif as UN (
  unify_terms)
import Data.Logic.ATP.DefCNF as DefCNF (
  defcnfs, defcnf2 )
import Data.Logic.ATP.Equate as EQ (
  FOL(..),
  EqAtom )
import Data.Logic.ATP.Quantified as QN (
  QFormula(..) )
import Data.Logic.ATP.Term as TR (
  Term(Var, FApply),
  V(..),
  FName,
  FTerm )
import Data.Logic.ATP.Apply as AP (
  Predicate )


import qualified Data.Set as S
import qualified Data.List as L
import qualified Data.Map as M
import Data.Function (on)
import Control.Monad.State.Strict (runState)
import Control.Monad.State.Lazy as STL (execStateT) 
import System.Exit (exitWith, ExitCode(ExitSuccess, ExitFailure))

import WemblAI (
  BeliefBase
  , TabTerm(..)
  , CDLTabTerm(..)
  , TableauxBranch(..)
  , EventId
  , EventP(..)
  , EventModelP(..)
  , Event(..)
  , EventModel(..)
  , WorldLabel
  , precond
  , altHst
  , expandL
  , tableauxStepTM
  , propify
  , unpropify
  , isImplied
  , isConsistent
  )
import BeliefRevision as BR
  
import qualified WemblAI as WAI (
  CDLFormula(..)
  , CDLFormulaP(Imp)
  , CDLFormulaP(And)
  , CDLFormulaP(Or)
  , CDLFormulaP(Not)
  , CDLFormulaP(Atom)
  , CDLFormulaP(Bel)
  , CDLFormulaP(Act)
  , CDLFormulaP(T)
  , CDLFormulaP(F)
  , EventModelP(EventModelP)
  , ModNumProp(..)
  , NumProp(..)
  , LitEffect(..)
  )  

strings :: Pretty a => S.Set (S.Set a) -> [[String]]
-- strings ss = sortBy (compare `on` length) . sort . S.toList $ S.map (sort . S.toList . S.map prettyShow) ss
strings = (L.sortBy (compare `on` length)) . L.sort . S.toList . (S.map (L.sort . S.toList . S.map prettyShow))

{--
pc_at_sight = NP { pname = "pc_at_sight", idx = 0 }
p_pc_at_sight = Prop.Atom pc_at_sight
l_pc_at_sight = Lit.Atom pc_at_sight
pc_badly_dressed = NP { pname = "pc_badly_dressed", idx = 1 }
p_pc_badly_dressed = Prop.Atom pc_badly_dressed
l_pc_badly_dressed = Lit.Atom pc_badly_dressed
pc_attacked = NP { pname = "pc_attacked", idx = 2 }
p_pc_attacked = Prop.Atom pc_attacked
l_pc_attacked = Lit.Atom pc_attacked

pc_has_weapons = NP { pname = "pc_has_weapons", idx = 3 }
p_pc_has_weapons = Prop.Atom pc_has_weapons
l_pc_has_weapons = Lit.Atom pc_has_weapons
pc_is_weaker = NP { pname = "pc_is_weaker", idx = 4 }
p_pc_is_weaker = Prop.Atom pc_is_weaker
l_pc_is_weaker = Lit.Atom pc_is_weaker

-- Imp ( (And pc_badly_dressed (Not pc_has_weapons)) pc_is_weaker )
-- Not an implication! rather, a Ba(P/Q)

npc_has_loot = NP { pname = "npc_has_loot", idx = 5 }
p_npc_has_loot = Prop.Atom npc_has_loot
l_npc_has_loot = Lit.Atom npc_has_loot
npc_at_door = NP { pname = "npc_at_door", idx = 6 }
p_npc_at_door = Prop.Atom npc_at_door
l_npc_at_door = Lit.Atom npc_at_door

guard_mind_now = Mind {
  know = fromList [
    Lit.Atom pc_at_sight,
    Lit.Not (Lit.Atom pc_attacked),
    Lit.Atom npc_at_door,
    Lit.Not (Lit.Atom npc_has_loot)
  ],
  believe = guards_believes_1}
  
-- The first task is to build beliefs according to knowledge!
-- If we don't know anything, and we learn new items, how does affect our
-- beliefs?
-- Let's start by trying out a "final state"
-- Implication as a rule? Not directly played out, but rather used as a "rule"
-- to create new belief states. 

guards_believes_1 = [
    Prop.Imp (p_pc_badly_dressed) (Prop.And (Prop.Not (p_pc_has_weapons)) (p_pc_is_weaker) ),
    Prop.Imp (p_pc_badly_dressed) (p_pc_is_weaker)
  ]

guards_believes_2 = [
    Prop.And (Prop.Atom pc_is_weaker) (Prop.Not (Prop.Atom pc_has_weapons)),
    Prop.And (Prop.Not (Prop.Atom pc_is_weaker)) (Prop.Atom pc_has_weapons)
  ]

-- hierarchies of application? take bottom layer and apply a "separate according
-- to evidence" operator

-- The final layer belongs to the "not pc_is_weaker" worlds, regardless of
-- whether the PC has weapons

-- How did we build this? initially the list of layers is [], which means that
-- all believes are basically equally believable
-- Regardless of whether the person flashes a blade, the guard will still think
-- the newcomer is 

-- pc_badly_dressed and not_pc_attacked must induce the belief structure seen
-- above

-- pc_badly_dressed induces pc_is_weaker layer
-- not pc_attacked induces not pc_has_weapons

-- First, check Knows for SAT
-- Then, start checking beliefs from most to less plausible
-- We can modify a belief set  

test1 :: Test
test1 =
    let
      expected = "[atomic pc_badly_dressed p1 .=>. (.~.) atomic pc_has_weapons p3 .&. atomic pc_is_weaker p4,atomic pc_badly_dressed p1 .=>. atomic pc_is_weaker p4]"
      input = show guards_believes_1
        in
    TestCase $ assertEqual "Simple show" expected input

test2 :: Test
test2 =
    let
      -- "[atomic pc_badly_dressed p1 .&. atomic pc_has_weapons p3 .&. (atomic pc_badly_dressed p1 .=>. atomic pc_is_weaker p4)]"
      expected = "[fromList [fromList [Atom pc_badly_dressed p1],fromList [Atom pc_has_weapons p3],fromList [Atom pc_is_weaker p4,Not (Atom pc_badly_dressed p1)]]]"
      cnfOut = L.map (cnf') (applyF (Prop.And p_pc_badly_dressed p_pc_has_weapons) guards_believes_1)
      input = show cnfOut 
        in
    TestCase $ assertEqual "Use of cnf' after hard apply" expected input

test3 :: Test
test3 =
    let
      -- "[atomic pc_badly_dressed p1 .&. atomic pc_has_weapons p3 .&. (atomic pc_badly_dressed p1 .=>. atomic pc_is_weaker p4)]"
      expected = "[fromList [fromList [Atom pc_badly_dressed p1],fromList [Atom pc_has_weapons p3],fromList [Atom pc_is_weaker p4,Not (Atom pc_badly_dressed p1)]]]"
      cnfOut :: [Set (Set BLiteral)]
      cnfOut = L.map (defcnfs) (applyF (Prop.And p_pc_badly_dressed p_pc_has_weapons) guards_believes_1)
      input = show cnfOut 
        in
    TestCase $ assertEqual "Use of defcnfs after hard apply" expected input

test4 :: Test
test4 =
    let
      expected = ""
      input = show $ hardUpdate (S.singleton l_pc_badly_dressed) guard_mind_now
        in
    TestCase $ assertEqual "Hard update" expected input

-- Add a test with TWO non believe terms
--}

-- CDL formulae
p = WAI.Atom WAI.NP{WAI.pname = "p", WAI.idx = 1}
q = WAI.Atom WAI.NP{WAI.pname = "q", WAI.idx = 2}
r = WAI.Atom WAI.NP{WAI.pname = "r", WAI.idx = 3}
s = WAI.Atom WAI.NP{WAI.pname = "s", WAI.idx = 4}
notp = WAI.Not p
notq = WAI.Not q
notr = WAI.Not r
nots = WAI.Not s
anne = "Anne"
bob = "Bob"
aisha = "Aisha"
boYang = "BoYang"
chinira = "Chinira"

-- Prop formulae
pp :: Prop.PFormula Prop.Prop
pp = Prop.Atom (Prop.P "pp")
notpp = Prop.Not pp
qq :: Prop.PFormula Prop.Prop
qq = Prop.Atom (Prop.P "qq")
notqq = Prop.Not qq


testActionSet :: Test
testActionSet =
  let
    expected = ""
    input = ""
    -- tab = tableauxStepM [] terms
    anneKROrS = WAI.Bel anne (WAI.Or r s)
    bobKPAndQ = WAI.Bel bob (WAI.And p q)
    anneKBobNKr = WAI.Bel anne (WAI.Not (WAI.Bel bob r)) 
    bobKNAnneKROrs = WAI.Not (WAI.Bel bob (WAI.Bel anne (WAI.Or r s)))
    actSet_1_2 = S.fromList [action1, action2]
    action1 = EventP {
      eventId = "action1",
      precond = WAI.T,
      -- postcond = (S.empty, S.empty),
      postcond = ([], S.empty),
      model = evtM
    }
    action2 = EventP {
      eventId = "action2",
      precond = WAI.T,
      -- postcond = (S.empty, S.empty),
      postcond = ([], S.empty),
      model = evtM
    }
    evtM :: EventModel
    evtM = EventModelP {
      eventModelId = "evtM",
      modelEvents = [[(action1, 8), (action2, 8)]]
    }
      in
    TestCase $ assertEqual "complete tableaux procedure 2 (monadic)" expected input
    
testTableauxTM1 :: Test
testTableauxTM1 =
  let
    expected = ""
    proofTree :: Tree ([CDLTabTerm], Bool)
    finalWorld :: WorldLabel
    (proofTree, finalWorld) = runState tab 0
    input = (show finalWorld) ++ ":\n" ++ (drawTree $ fmap (show) (proofTree))
    tab = tableauxStepTM [] [[terms]] (Node ([], False) [])
    terms = [After 0 [] (WAI.And p q),
             After 0 [] (WAI.Not (WAI.Not (WAI.Not p))),
             After 0 [] (WAI.Not (WAI.Bel anne q)),
             After 0 [] (WAI.Not (WAI.Bel anne s)),
             After 0 [] (WAI.Or r s)]
      in
    TestCase $ assertEqual "test for single term lists (tree monadoc)" expected input

testExpandEvents :: Test
testExpandEvents =
  let
    expected = ""
    input = show $ altHst anne [(model evt1, evt1), (model evt2, evt2)]
    evt1 :: Event
    evt1 = EventP {
      eventId = "evt1",
      precond = WAI.Bel anne p,
      postcond = ([], S.empty),
      model = evtM
    }  
    evt2 :: Event
    evt2 = EventP {
      eventId = "evt2",
      precond = WAI.Bel anne q,
      postcond = ([], S.empty),
      model = evtM
    }  
    evt3 :: Event
    evt3 = EventP {
      eventId = "evt3",
      precond = WAI.Bel anne r,
      postcond = ([], S.empty),
      model = evtM
    }
    evtM :: EventModel
    evtM = EventModelP {
      eventModelId = "evtM",
      modelEvents = [[(evt1, 5), (evt2, 5), (evt3, 6)]]
    }
      in
    TestCase $ assertEqual "event expansion for use in tableux terms" expected input

testBaRule :: Test
testBaRule =
  let
    expected = ""
    (proofTree, isProven) = runState tab (0 :: WorldLabel)
    input = (show isProven) ++ ":\n" ++ (drawTree $ fmap (show) (proofTree))
    tab = tableauxStepTM [] [[terms]] (Node ([], False) [])
    terms = [After 1 [] (WAI.And p q),
             Access anne (0,1),
             After 0 [(model evt1, evt1), (model evt2, evt2)] (WAI.Bel anne q)]
    evt1 :: Event
    evt1 = EventP {
      eventId = "evt1",
      precond = WAI.Bel anne p,
      postcond = ([], S.empty),
      model = evtM
    }  
    evt2 :: Event
    evt2 = EventP {
      eventId = "evt2",
      precond = WAI.Bel anne q,
      postcond = ([], S.empty),
      model = evtM
    }  
    evtM :: EventModel
    evtM = EventModelP {
      eventModelId = "evtM",
      modelEvents = [[(evt1, 8), (evt2, 8)]]
    }
      in
    TestCase $ assertEqual "test for Ba rule in an event history" expected input

testNegBaRule :: Test
testNegBaRule =
  let
    expected = ""
    proofTree :: Tree ([CDLTabTerm], Bool)
    isProven :: WorldLabel
    (proofTree, isProven) = runState tab 0
    prettyInput = (show isProven) ++ ":\n" ++ (drawTree $ fmap (show) (proofTree))
    input = show (isProven, proofTree)
    tab = tableauxStepTM [] [[terms]] (Node ([], False) [])
    terms = [After 0 [] (WAI.And p q),
             After 0 [(model evt1, evt1), (model evt2, evt2)] (WAI.Not (WAI.Bel anne q))]
    evt1 = EventP {
      eventId = "evt1",
      precond = WAI.Bel anne p,
      postcond = ([], S.empty),
      model = evtM
    }  
    evt2 = EventP {
      eventId = "evt2",
      precond = WAI.Bel anne q,
      postcond = ([], S.empty),
      model = evtM
    }  
    evtM :: EventModel
    evtM = EventModelP {
      eventModelId = "evtM",
      modelEvents = [[(evt1, 8), (evt2, 8)]]
    }
      in
    TestCase $ assertEqual ("Pretty print tree:\n" ++ prettyInput) expected input

testMuddyChildrenAfterAnnc :: Test
testMuddyChildrenAfterAnnc =
  let
    expected = ""
    proofTree :: Tree ([CDLTabTerm], Bool)
    isProven :: WorldLabel
    (proofTree, isProven) = runState tab 0
    prettyInput = (show isProven) ++ ":\n" ++ (drawTree $ fmap (show) (proofTree))
    input = (isProven, proofTree)
    tab = tableauxStepTM [] [[terms]] (Node ([], False) [])
    -- From PoV of Muddy1
    terms = [After 0 [] (WAI.Or m1 (WAI.Or m2 m3)),
             After 0 [] (WAI.Not m1),
             After 0 [] m2,
             After 0 [] (WAI.Not m3),
             After 0 [] (WAI.Not (WAI.Bel muddy2 m2)),
             After 0 [] (WAI.Bel muddy2 (WAI.Not m1)),
             After 0 [] (WAI.Bel muddy2 (WAI.Not m3)),
             After 0 [] (WAI.Bel muddy2 (WAI.Or m1 (WAI.Or m2 m3))),
             After 0 [] (WAI.Bel muddy3 (WAI.Not m1)),
             After 0 [] (WAI.Bel muddy3 m2),
             After 0 [] (WAI.Bel muddy3 (WAI.Or m1 (WAI.Or m2 m3))),
             After 0 [] (WAI.Not (WAI.Bel muddy3 m3))]
    m1 = WAI.Atom WAI.NP{WAI.pname = "m1", WAI.idx = 1}
    m2 = WAI.Atom WAI.NP{WAI.pname = "m2", WAI.idx = 2}
    m3 = WAI.Atom WAI.NP{WAI.pname = "m3", WAI.idx = 3}
    muddy1 = "Muddy1"
    muddy2 = "Muddy2"
    muddy3 = "Muddy3"
      in
    TestCase $ assertEqual ("Pretty print tree:\n" ++ prettyInput) expected (show input)

testModalFormulaTransf :: Test
propp = WAI.NP{WAI.pname = "p", WAI.idx = 1}
litp = Lit.Atom propp
setp    = [WAI.SetL propp]
setnotp = [WAI.UnsetL propp]
testModalFormulaTransf =
  let
    expected = ""
    modelAB = EventModelP {
      eventModelId = "modelAB",
      modelEvents = [[(eventA, 8)], [(eventB, 8)]]
    }
    eventA = EventP {
      eventId = "evtA",
      precond = WAI.T,
      postcond = (setp, S.empty),
      model = modelAB
    }
    eventB = EventP {
      eventId = "evtB",
      precond = WAI.T,
      postcond = (setnotp, S.empty),
      model = modelAB
    }
    input :: WAI.CDLFormula
    input = WAI.And
      (WAI.Or
        p
        (WAI.Bel aisha q))
      (WAI.Or
        r
        (WAI.Act (model eventA) eventA s))
--    propify :: CDLFormulaP NumProp Float String String ->
--                   State TransfMState (Prop.PFormula ModNumProp)
    (transformed, finalSt) = runState (propify input) (0, M.empty)
    info = "transformed: " ++ show transformed ++ "\n" ++
        "initial: " ++ show input ++ "\n" ++
        "map: " ++ show finalSt
      in
    TestCase $ assertEqual ("Test for transformation of modal formulas into prop like") expected info

testModalFormulaUnpropify :: Test
testModalFormulaUnpropify =
  let
    expected = ""
    modelAB = EventModelP {
      eventModelId = "modelAB",
      modelEvents = [[(eventA, 8), (eventB, 8)]]
    }
    eventA :: Event
    eventA = EventP {
      eventId = "evtA",
      precond = WAI.T,
      postcond = (setp, S.empty),
      model = modelAB
    }
    eventB :: Event
    eventB = EventP {
      eventId = "evtB",
      precond = WAI.T,
      postcond = (setnotp, S.empty),
      model = modelAB
    }
    input :: WAI.CDLFormula
    input = WAI.And
      (WAI.Or
        p
        (WAI.Bel aisha q))
      (WAI.Or
        r
        (WAI.Act (model eventA) eventA s))
--    propify :: CDLFormulaP NumProp Float String String ->
--                   State TransfMState (Prop.PFormula ModNumProp)
    (transformed, (_, fwdMap)) = runState (propify input) (0, M.empty)
    backToModal = unpropify (reverseMap fwdMap) transformed
    info = "original: " ++ show input ++ "\n" ++
        "unpropified: " ++ show backToModal
      in
    TestCase $ assertEqual ("Test for transformation of modal formulas into prop like") expected info

testCnfExamples1 :: Test
testCnfExamples1 =
  let
    expected = ""
    cdlFormula :: WAI.CDLFormula
    cdlFormula = WAI.Or s $ WAI.And notr $ WAI.And (WAI.Or p q) notp
    (transformed, _) = runState (propify cdlFormula) (0, M.empty)
    input :: S.Set (S.Set (Lit.LFormula WAI.ModNumProp))
    input = simpcnf id transformed
    in
      TestCase $ assertEqual ("Test for CNF simplification examples") expected (show input)

testCnfExamples2 :: Test
testCnfExamples2 =
  let
    expected = ""
    cdlFormula :: WAI.CDLFormula
    cdlFormula = WAI.And p notp
    (transformed, _) = runState (propify cdlFormula) (0, M.empty)
    input1 = dpll $ inputS1
    inputS1 :: S.Set (S.Set (Lit.LFormula Prop.Prop))
    inputS1 = simpcnf id $ Prop.And pp (Prop.Not pp)
    input2 = dpll $ inputS2
    inputS2 :: S.Set (S.Set (Lit.LFormula Prop.Prop))
    inputS2 = simpcnf id $ Prop.And pp (Prop.And pp pp)
    input3 = dpll $ inputS3
    inputS3 :: S.Set (S.Set (Lit.LFormula Prop.Prop))
    inputS3 = simpcnf id $ Prop.And notpp (Prop.Or pp qq)
    in
      TestCase $ assertEqual ("Test for CNF simplification examples") expected (show (inputS1, inputS2, inputS3))

testCnfExamples3 :: Test
testCnfExamples3 =
  let
    expected = ""
    cdlFormula :: WAI.CDLFormula
    cdlFormula = WAI.And p notp
    (transformed, _) = runState (propify cdlFormula) (0, M.empty)
    -- dpll :: (IsLiteral lit, Ord lit) => Set (Set lit) -> Bool
    input1 = dpll $ inputS1
    inputS1 :: S.Set (S.Set (Lit.LFormula Prop.Prop))
    inputS1 = simpcnf id $ Prop.And pp (Prop.Not pp)
    in
      TestCase $ assertEqual ("Test for per proposition change") expected (show inputS1)

-- Tableaux.tableau: find a way to unify with our tableau

testConsistentBase :: Test
testConsistentBase =
  let
    expected = ""
    input = ("Is nq consistent: " ++ show nqConsistent, "Is r consistent: " ++ show rConsistent)
    nqConsistent = isConsistent terms nq
    rConsistent = isConsistent terms r
    terms :: S.Set WAI.CDLFormula
    terms = S.fromList [p, pthq]
    p = WAI.Atom WAI.NP{WAI.pname = "p", WAI.idx = 1}
    q = WAI.Atom WAI.NP{WAI.pname = "q", WAI.idx = 2}
    r = WAI.Atom WAI.NP{WAI.pname = "r", WAI.idx = 3}
    np = WAI.Not p
    nq = WAI.Not q
    pthq = WAI.Imp p q
      in
    TestCase $ assertEqual ("testConsistentBase") expected (show input)
    
testIsImplied :: Test
testIsImplied =
  let
    expected = ""
    input = ("Is q implied: " ++ show qImplied, "Is r implied: " ++ show rImplied)
    qImplied = isImplied terms q
    rImplied = isImplied terms r
    terms :: S.Set WAI.CDLFormula
    terms = S.fromList [p, pthq]
    p = WAI.Atom WAI.NP{WAI.pname = "p", WAI.idx = 1}
    q = WAI.Atom WAI.NP{WAI.pname = "q", WAI.idx = 2}
    r = WAI.Atom WAI.NP{WAI.pname = "r", WAI.idx = 3}
    np = WAI.Not p
    nq = WAI.Not q
    pthq = WAI.Imp p q
      in
    TestCase $ assertEqual ("testIsImplied") expected (show input)

testDummyIsImplied :: Test
testDummyIsImplied =
  let
    expected = ""
    input = ("Is T implied from []: " ++ show tImplied, "Is F implied from []: " ++ show fImplied)
    tImplied = isImplied S.empty (WAI.T :: WAI.CDLFormula)
    fImplied = isImplied S.empty (WAI.F :: WAI.CDLFormula)
      in
    TestCase $ assertEqual ("testDummyIsImplied") expected (show input)

testMinBelSet1 :: Test
testMinBelSet1 =
  let
    expected = ""
    input = minBelSet belSet
    p = WAI.Atom WAI.NP{WAI.pname = "p", WAI.idx = 1}
    q = WAI.Atom WAI.NP{WAI.pname = "q", WAI.idx = 2}
    r = WAI.Atom WAI.NP{WAI.pname = "r", WAI.idx = 3}
    s = WAI.Atom WAI.NP{WAI.pname = "s", WAI.idx = 4}
    notq = WAI.Not q
    pthenq = WAI.Imp p q
    belSet :: BeliefBase WAI.NumProp Integer String String
    belSet = [(S.fromList [p, notq], 7),
              (S.fromList [pthenq], 4),
              (S.fromList [r], 1)]
      in
  TestCase $ assertEqual ("Test for belief set 1") expected (show input)

testMinBelSet2 :: Test
testMinBelSet2 =
  let
    expected = ""
    input = minBelSet belSet
    p = WAI.Atom WAI.NP{WAI.pname = "p", WAI.idx = 1}
    q = WAI.Atom WAI.NP{WAI.pname = "q", WAI.idx = 2}
    r = WAI.Atom WAI.NP{WAI.pname = "r", WAI.idx = 3}
    s = WAI.Atom WAI.NP{WAI.pname = "s", WAI.idx = 4}
    notq = WAI.Not q
    pthenq = WAI.Imp p q
    belSet :: BeliefBase WAI.NumProp Integer String String
    belSet = [(S.fromList [p, notq], 7),
              (S.fromList [r], 4),
              (S.fromList [pthenq], 1)]
  in
    TestCase $ assertEqual ("Test for belief set 2") expected (show input)

-- FIXME finish this
testUnify1 :: Test
testUnify1 = 
  let
    expected = ""
    input = ""
    blockA :: TR.FTerm
    blockA = TR.FApply (fromString "A") []
    blockB :: TR.FTerm
    blockB = TR.FApply (fromString "B") []
    blockC :: TR.FTerm
    blockC = TR.FApply (fromString "C") []
    blockD :: TR.FTerm
    blockD = TR.FApply (fromString "D") []

    varX :: TR.FTerm
    varX = TR.Var (fromString "x")
    varY :: TR.FTerm
    varY = TR.Var (fromString "y")

    -- (:INIT (CLEAR C) (CLEAR A) (CLEAR B) (CLEAR D) (ONTABLE C) 
    -- (ONTABLE A) (ONTABLE B) (ONTABLE D) (HANDEMPTY))

    handempty :: EQ.EqAtom
    handempty = EQ.R (fromString "handempty") []
    clearA :: EQ.EqAtom
    clearA = EQ.R (fromString "clear") [blockA]
    clearB :: EQ.EqAtom
    clearB = EQ.R (fromString "clear") [blockB]
    clearC :: EQ.EqAtom
    clearC = EQ.R (fromString "clear") [blockC]
    clearD :: EQ.EqAtom
    clearD = EQ.R (fromString "clear") [blockD]
    ontableA :: EQ.EqAtom
    ontableA = EQ.R (fromString "ontable") [blockA]
    ontableB :: EQ.EqAtom
    ontableB = EQ.R (fromString "ontable") [blockB]
    ontableC :: EQ.EqAtom
    ontableC = EQ.R (fromString "ontable") [blockC]
    ontableD :: EQ.EqAtom
    ontableD = EQ.R (fromString "ontable") [blockD]
    clearX :: EQ.EqAtom
    clearX = EQ.R (fromString "clear") [varX]
    ontableX :: EQ.EqAtom
    ontableX = EQ.R (fromString "ontable") [varX]
    holdingX :: EQ.EqAtom
    holdingX = EQ.R (fromString "holding") [varX]

    varMap :: IO (M.Map TR.V FTerm)
    varMap = do 
      someMap <- STL.execStateT (UN.unify_terms [(blockA, varX)]) M.empty
      return someMap

    -- UN.unify_atoms_eq clearA clearX
    {--
    (:predicates (on ?x - block ?y - block)
    (ontable ?x - block)
    (clear ?x - block)
    (handempty)
    (holding ?x - block)
    )
    --}

    -- Try a sequent calculus for FOL
    {--
    (:action pick-up
    :parameters (?x - block)
    :precondition (and (clear ?x) (ontable ?x) (handempty)) --}
    precond = QN.And
      (QN.Atom handempty)
      (QN.And
        (QN.Atom clearX)
        (QN.Atom ontableX) )
    -- :effect
    pickUpEffects = [
      -- (and (not (ontable ?x))
      QN.Not (QN.Atom ontableX),
      -- (not (clear ?x))
      QN.Not (QN.Atom clearX),
      -- (not (handempty))
      QN.Not (QN.Atom handempty),
      -- (holding ?x)))
      QN.Not (QN.Atom holdingX)]
    in
      TestCase $ assertEqual ("Test for unification") expected (show input)
  
    {--
(:action put-down
    :parameters (?x - block)
    :precondition (holding ?x)
    :effect
    (and (not (holding ?x))
    (clear ?x)
    (handempty)
    (ontable ?x)))
(:action stack
    :parameters (?x - block ?y - block)
    :precondition (and (holding ?x) (clear ?y))
    :effect
    (and (not (holding ?x))
    (not (clear ?y))
    (clear ?x)
    (handempty)
    (on ?x ?y)))
(:action unstack
    :parameters (?x - block ?y - block)
    :precondition (and (on ?x ?y) (clear ?x) (handempty))
    :effect
    (and (holding ?x)
    (clear ?y)
    (not (clear ?x))
    (not (handempty))
    (not (on ?x ?y)))))
    
(:goal (AND (ON D C) (ON C B) (ON B A)))    --}

    -- R (NamedPred ) [, ] :: EqAtom
    -- data QFormula v atom
    -- type EqFormula = QFormula V EqAtom
    -- A quantified formula with a certain variable and a certain atom
    -- type EqAtom = FOL Predicate FTerm
    -- data FOL predicate term = R predicate [term] | Equals term term deriving (Eq, Ord, Data, Typeable, Read)
    -- data Predicate = NamedPred String
    -- type FTerm = Term FName V
    -- data Term function v
    -- = Var v
    -- | FApply function [Term function v]
    -- deriving (Eq, Ord, Data, Typeable, Read)

                                                        
main :: IO ()
main = runTestTT (TestList  [-- TestLabel "Simple show" test1,
                             -- TestLabel "Use of cnf' after hard apply" test2,
                             -- TestLabel "Use of defcnfs after hard apply" test3,
                             -- TestLabel "Hard update" test4,
                             -- TestLabel "test for single term lists" testTableauxTM1,
                             -- TestLabel "event expansion for use in tableux terms" testExpandEvents,
                             -- TestLabel "test for Ba rule in an event history" testBaRule,
                             -- TestLabel "test for neg Ba rule with an event history" testNegBaRule,
                             -- TestLabel "test for muddy children after first announcement, m1 muddy and observing" testMuddyChildrenAfterAnnc,
                             -- TestLabel "Test for transformation of modal formulas into prop like" testModalFormulaTransf,
                             TestLabel "Test for reversing prop like formulas back into modal" testModalFormulaUnpropify,
                             TestLabel "Test for a consistent base" testConsistentBase,
                             TestLabel "Test for an implying base" testIsImplied,
                             TestLabel "Test for an dummy implies" testDummyIsImplied,
                             TestLabel "Test for unification" testUnify1
                             -- TestLabel "Test for belief set 1" testMinBelSet1,
                             -- TestLabel "Test for belief set 2" testMinBelSet2
                             -- TestLabel "Test for CNF simplification examples 1" testCnfExamples1,
                             -- TestLabel "Test for CNF simplification examples 2" testCnfExamples2
                             ]) >>= doCounts
    where
      doCounts counts' = exitWith (if errors counts' /= 0 || failures counts' /= 0 then ExitFailure 1 else ExitSuccess)
