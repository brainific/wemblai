import Test.HUnit
import Hanabi

import System.Random
import System.Exit (exitWith, ExitCode(ExitSuccess, ExitFailure))

testHnb :: Test
testHnb =
  let
    expected = ""
    gen1 = mkStdGen 10
    orig = [1,2,3,4,5,6,7,8,9]
    (gen2, hd1, tl1) = rndSplit gen1 orig
    (gen3, hd2, tl2) = rndSplit gen2 orig
    input = show ((hd1, tl1), (hd2, tl2))
    in
      TestCase $ assertEqual "Hanabi test" expected input
  
testDeck :: Test
testDeck =
  let
    expected = ""
    input = show (buildDeck)
    in
      TestCase $ assertEqual "Hanabi deck test" expected input
  
testShuffleDeck :: Test
testShuffleDeck =
  let
    expected = ""
    gen1 = mkStdGen 5
    input = show (shuffleDeck gen1 buildDeck)
    in
      TestCase $ assertEqual "Hanabi shuffle deck test" expected input
  
testGameSetup :: Test
testGameSetup =
  let
    expected = ""
    gen1 = mkStdGen 100
    shuffled1 = shuffleDeck gen1 buildDeck
    (hand1, shuffled2) = splitAt 5 shuffled1
    (hand2, shuffled3) = splitAt 5 shuffled2
    input = show (hand1, hand2)
    in
      TestCase $ assertEqual "Hanabi game setup test" expected input

      
buildGame gen1 = (hand1, hand2, shuffled1)
  where
    shuffled1 = shuffleDeck gen1 buildDeck
    (hand1, shuffled2) = splitAt 5 shuffled1
    (hand2, shuffled3) = splitAt 5 shuffled2

buildHints hand1 hand2 = (hintsNum1, hintsSuits1, hintsNum2, hintsSuits2)
  where
    hintsNum1 = map (\n -> (n, whichNum hand1 n)) [1..5]
    hintsSuits1 = map (\s -> (s, whichSuit hand1 s)) suits
    hintsNum2 = map (\n -> (n, whichNum hand2 n)) [1..5]
    hintsSuits2 = map (\s -> (s, whichSuit hand2 s)) suits

testHints :: Test
testHints =
  let
    (hand1, hand2, deck) = buildGame gen1
    expected = ""
    gen1 = mkStdGen 100
    (hintsNum1, hintsSuits1, hintsNum2, hintsSuits2) = buildHints hand1 hand2
    input = show (hintsNum1, hintsSuits1, hintsNum2, hintsSuits2)
    in
      TestCase $ assertEqual "Hanabi giving hints test" expected input
  
testBuildHints :: Test
testBuildHints =
  let
    (hand1, hand2, deck) = buildGame gen1
    expected = ""
    gen1 = mkStdGen 100
    (hintsNum1, hintsSuits1, hintsNum2, hintsSuits2) = buildHints hand1 hand2
    input = show (hints1_1, hints1_2, hints2_1, hints2_2)
    hints1_1 = useNHints hintsNum1 emptyHints
    hints1_2 = useSHints hintsSuits1 hints1_1
    hints2_1 = useNHints hintsNum2 emptyHints
    hints2_2 = useSHints hintsSuits2 hints2_1
    in
      TestCase $ assertEqual "Hanabi building hints test" expected input

testBuildGame :: Test
testBuildGame =
  let
    gen1 = mkStdGen 1
    (hand1, hand2, deck) = buildGame gen1
    hintsNum1 = map (\n -> (n, whichNum hand1 n)) [1..5]
    hintsSuits1 = map (\s -> (s, whichSuit hand1 s)) suits
    played = [Card s 0 | s <- suits]
    expected = ""
    input = show hand2 ++ "\n" ++
            show hintsNum1 ++ "\n" ++
            show hintsSuits1 ++ "\n" ++
            show played ++ "\n" ++
            "(" ++ show hand1 ++ ")"

    in
      TestCase $ assertEqual "Hanabi building game" expected input

testPlayCardNok :: Test
testPlayCardNok =
  let
    game1 = initGame 1
    [hand1, hand2] = hands game1
    (hintsNum1, hintsSuits1, hintsNum2, hintsSuits2) = buildHints hand1 hand2
    Just game2 = playCard 0 0 game1
    expected = game1
    input = game2
    in
      TestCase $ assertEqual "Hanabi play a card (incorrectly)" expected input

testPlayCardOk :: Test
testPlayCardOk =
  let
    game1 = initGame 1
    [hand1, hand2] = hands game1
    (hintsNum1, hintsSuits1, hintsNum2, hintsSuits2) = buildHints hand1 hand2
    Just game2 = playCard 0 3 game1
    expected = game1
    input = game2
    in
      TestCase $ assertEqual "Hanabi play a card (correctly)" expected input

{--      
testDiscard :: Test
testDiscard =
  let
    gen1 = mkStdGen 1
    (hand1, hand2, deck) = buildGame gen1
    hintsNum1 = map (\n -> (n, whichNum hand1 n)) [1..5]
    hintsSuits1 = map (\s -> (s, whichSuit hand1 s)) suits
    played = [Card s 0 | s <- suits]
    expected = ""
    input = show hand2 ++ "\n" ++
            show hintsNum1 ++ "\n" ++
            show hintsSuits1 ++ "\n" ++
            show played ++ "\n" ++
            "(" ++ show hand1 ++ ")"

    in
      TestCase $ assertEqual "Hanabi building game" expected input

testPlayHint :: Test
testPlayHint =
  let
    gen1 = mkStdGen 1
    (hand1, hand2, deck) = buildGame gen1
    hintsNum1 = map (\n -> (n, whichNum hand1 n)) [1..5]
    hintsSuits1 = map (\s -> (s, whichSuit hand1 s)) suits
    played = [Card s 0 | s <- suits]
    expected = ""
    input = show hand2 ++ "\n" ++
            show hintsNum1 ++ "\n" ++
            show hintsSuits1 ++ "\n" ++
            show played ++ "\n" ++
            "(" ++ show hand1 ++ ")"

    in
      TestCase $ assertEqual "Hanabi building game" expected input
--}

main :: IO ()
main = runTestTT (TestList  [TestLabel "Hanabi test" testHnb,
                             TestLabel "Hanabi deck test" testDeck,
                             TestLabel "Hanabi shuffle deck test" testShuffleDeck,
                             TestLabel "Hanabi game setup test" testGameSetup,
                             TestLabel "Hanabi giving hints test" testHints,
                             TestLabel "Hanabi building hints test" testBuildHints,
                             TestLabel "Hanabi building game" testBuildGame,
                             TestLabel "Hanabi play a card (incorrectly)" testPlayCardNok,
                             TestLabel "Hanabi play a card (correctly)" testPlayCardOk
                             ]) >>= doCounts
    where
      doCounts counts' = exitWith (if errors counts' /= 0 || failures counts' /= 0 then ExitFailure 1 else ExitSuccess)
