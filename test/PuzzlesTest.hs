{-# LANGUAGE TypeFamilies #-}

import Test.HUnit
import qualified Data.Set as S
import qualified Data.List as L
import qualified Data.Map as M

import qualified Moves as MV

import qualified ATP.FOL as FOL

-- If we can, we'll move everything to FOL
-- type TestAtom = NumProp
type TestAtom = EqAtom
type TestPlaus = Plaus16
type TestAgent = String
type TestEvtid = String
type TestTime = Integer
type TestActName = String
type TestVar = String
-- type TestVar = V

-- type EqFormula = QFormula V EqAtom
-- data QFormula v atom
-- data FOL predicate term = R predicate [term] | Equals term term deriving (Eq, Ord, Data, Typeable, Read)
-- type EqAtom = FOL Predicate FTerm
-- -> R (NamedPred "has_a") [Var (V "x"), FApply (FName "a") []] :: EqAtom
-- data Term function v = Var v | FApply function [Term function v]
-- newtype V = V String deriving (Eq, Ord, Data, Typeable, Read)
-- type FTerm = Term FName V
-- newtype FName = FName String deriving (Eq, Ord)


type TestAndNode = AndNode TestAtom TestPlaus TestAgent TestEvtid TestTime
-- AndNode atom plaus agent evtid time
type TestOrNode = OrNode TestAtom TestPlaus TestAgent TestEvtid TestTime
-- OrNode atom plaus agent evtid time
type TestBeliefBase = BeliefBase TestAtom TestPlaus TestAgent TestEvtid
-- BeliefBase atom plaus agent evtid
type TestCDLFormula = CDLFormulaP TestAtom TestPlaus TestAgent TestEvtid
-- CDLFormulaP atom plaus agent evtid
type TestEventModel = EventModelP TestAtom TestPlaus TestAgent TestEvtid
-- EventModelP atom plaus agent evtid
type TestMove = Move var TestAtom TestActName TestPlaus String
-- Move var atom actname plaus agent
type TestVar = String

aisha = "Aisha"
boYang = "BoYang"
chinira = "Chinira"

-- Traditional plan recognition: record observations, search in
-- plan library for plans that correspond to observations
-- Ramírez, Geffner 2009: build the plan library
-- through the use of sub/optimal planners
-- issue: epistemic actions: what is the state (complex actions à la McIlraith?)
-- c->b, b->a, ... new fluents representing "having done" an action previously
-- New actions in observations (b -> a) have:
-- * Add(a) U {pa}
-- * Pre(a) U {pb}
-- Ha, Rowe 2014: MLNs, but with hand constructed formulae, and grounded independently
-- Geib, Goldman 2009: PHATT
{--
sigma: actions, SIGMA = {actions}
First, for every terminal in the grammar, plan tree grammars must have a distinguished
non-terminal (captured in the set TNT) that maps uniquely to the terminal symbol.
The set NNT captures the non-terminals in the grammar that do not uniquely map to a terminal.
Note that root goals, members of R, may appear on the right hand side of a production. That is,
root nodes are those that are permitted to appear at the top of a derivation tree; they are not
required to appear only at the top of a derivation tree.

Procedure 6.1 (Top-down explanation generation)
  PROCEDURE Explain({sigma1..sigmaN})
  CHOOSE initial goal hypothesis D0 from R;
  E = <D0; {PS(D0)}>;
  LOOP FOR i = 1 to n DO
    CHOOSE Tnew € PS(Di-1) such that foot(Tnew) = sigmai;
    Di =Substitute(Tnew,Di-1);
    E = <Di; {PS(D0)..PS(Di)}>;
  END LOOP;
RETURN E;
--}

testPuzzle1 :: Test
testPuzzle1 =
{--
A says, "C said ..."
B reasons that C wants <goal>, so B acts against what A said
B doesnt want to ask C because C will deny anyway
Aisha says that Chinira wants Bo Yang to fetch some mystical books from a nearby castle
This may or may not be true, as long as Aisha is sure that Bo Yang will not check
the information himself
--}
  let
    expected = ""
    -- We're in turn 1, say
    aSaid = Say
      aisha
      (Past 0
        (Request
          chinira
          (Request boYang (Does boYang "fetchBooks"))
          (S.singleton aisha)
        )
      )
      (S.singleton boYang)
    -- plan/goal recognition: C wants/plans something , from announced actions
    -- -> fetchBooks has different effects:
    actFetchBooks = ActionSpec "fetchBooks" (S.fromList([
      -- BoYang is injured
      (Result
        S.fromList([
          SetEff "boYangInjured"
        ])
        S.fromList([
          Senses chinira "boYangInjured",
          Senses chinira "booksInCastle"
          -- Check how to express observability for FO
          -- Senses chinira "booksInCastle"
        ]),
        Plaus16 3),
      -- The books are here and some days have passed  
      (Result
        S.fromList([
          SetEff "booksHere"
        ])
        S.fromList([
          Senses chinira "boYangDies",
          Senses chinira "booksAway"
        ]),
        Plaus16 3),
      -- The books have not been found and some days have passed  
      (Result
        S.fromList([
          SetEff "booksNotHere"
        ])
        S.fromList([
          Senses chinira "boYangDies",
          Senses chinira "booksAway"
        ]),
        Plaus16 3)
    ]))
    --    * books here -> Chinira uses books to summon demon
    --                 -> Chinira uses books to fortify base
    --                 -> Chinira uses books to research mysticism
    --    * boYang is delayed
    --    * boYang is hurt
    --    * boYang finds something else
    -- B can do several things:
    -- 

    belBase :: TestBeliefBase 
    belBase = [(S.fromList [unharmedCdl], Plaus16 1),
               (S.fromList [topStairsCdl, lightOffCdl], Plaus16 2),
               (S.fromList [switchOffCdl], Plaus16 3),
               (S.fromList [bulbOkCdl], Plaus16 5),
               (S.fromList [switchOnCdl, topStairsCdl], Plaus16 7)]
    (know, prioBels) = kandb belBase
  in
    TestCase $ assertEqual ("Puzzle 1") expected (show $ (
      prioBels,
      sos prioBels,
      possibles
    ))

testPuzzle2 :: Test
testPuzzle2 =
{--
A says something that is false
B reasons that A wants B to act in some other way
B comes up with some other info to guide A
--}
  let
    expected = ""
    belBase :: TestBeliefBase 
    belBase = [(S.fromList [unharmedCdl], Plaus16 1),
               (S.fromList [topStairsCdl, lightOffCdl], Plaus16 2),
               (S.fromList [switchOffCdl], Plaus16 3),
               (S.fromList [bulbOkCdl], Plaus16 5),
               (S.fromList [switchOnCdl, topStairsCdl], Plaus16 7)]
    (know, prioBels) = kandb belBase
  in
    TestCase $ assertEqual ("Puzzle 1") expected (show $ (
      prioBels,
      sos prioBels,
      possibles
    ))
    