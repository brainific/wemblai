{-# LANGUAGE TypeFamilies #-}

import Test.HUnit
import Data.Tree
import Data.Logic.ATP.FOL (EqFormula)
import Data.Logic.ATP.Pretty (Pretty(pPrint), prettyShow)
import qualified Data.Logic.ATP.Prop as Prop (
  Prop(P, pname),
  PFormula(..),
  psimplify,
  testProp,
  dnf,
  cnf',
  simpcnf,
  simpdnf,
  (∨),
  (∧),
  (.<=>.),
  (.&.),
  (.|.),
  BinOp(..) )
import qualified Data.Logic.ATP.DP as DP (
  dpllsat, dpll )
import qualified Data.Logic.ATP.Lit as Lit (
  LFormula(..),
  IsLiteral(..),
  negate )
import qualified Data.Logic.ATP.Formulas as Formulas (
  IsFormula(..),
  IsAtom )
import Data.Logic.ATP.DefCNF as DefCNF (
  defcnfs, defcnf2 )

import qualified Data.Set as S
import qualified Data.List as L
import qualified Data.Map as M
import Data.Function (on)
import Control.Monad.State.Strict (runState)
import System.Exit (exitWith, ExitCode(ExitSuccess, ExitFailure))

import BeliefRevision as BR

import Data.Ord (Ordering(..))

import DelPlanner as PL
  
import WemblAI (
  CDLFormulaP(..)
  , LitEffect(..)
  , EventModelP(..)
  , EventP(..)
  , Plaus16(..)
  , NumProp(..)
  , BeliefBase(..)
  , simplify
  , propify
  , propifyAtom
  , propifyEffect
  , propifySet
  , unpropify
  , reverseMap
  , isImplied
  , isConsistent
  , mapFst
  , subsumes
  , subsumptionStep
  , implyStep
  , cnfStateMerge
  , maxP
  , plausToProb
  , checkPrecond
  )
import qualified WemblAI as WAI (
  ModNumProp(..)
  , lit2Cdl
  , isConsistent
  )

type TestAndNode = AndNode NumProp Plaus16 String String Float 
type TestOrNode = OrNode NumProp Plaus16 String String Float
type TestBeliefBase = BeliefBase NumProp Plaus16 String String
type TestCDLFormula = CDLFormulaP NumProp Plaus16 String String
type TestEventModel = EventModelP NumProp Plaus16 String String

strings :: Pretty a => S.Set (S.Set a) -> [[String]]
-- strings ss = sortBy (compare `on` length) . sort . S.toList $ S.map (sort . S.toList . S.map prettyShow) ss
strings = (L.sortBy (compare `on` length)) . L.sort . S.toList . (S.map (L.sort . S.toList . S.map prettyShow))

pc_at_sight = NP { pname = "pc_at_sight", idx = 0 }
p_pc_at_sight = Prop.Atom pc_at_sight
l_pc_at_sight = Lit.Atom pc_at_sight

pc_badly_dressed = NP { pname = "pc_badly_dressed", idx = 1 }
p_pc_badly_dressed = Prop.Atom pc_badly_dressed
l_pc_badly_dressed = Lit.Atom pc_badly_dressed

pc_attacked = NP { pname = "pc_attacked", idx = 2 }
p_pc_attacked = Prop.Atom pc_attacked
l_pc_attacked = Lit.Atom pc_attacked

pc_has_weapons = NP { pname = "pc_has_weapons", idx = 3 }
p_pc_has_weapons = Prop.Atom pc_has_weapons
l_pc_has_weapons = Lit.Atom pc_has_weapons

pc_is_weaker = NP { pname = "pc_is_weaker", idx = 4 }
p_pc_is_weaker = Prop.Atom pc_is_weaker
l_pc_is_weaker = Lit.Atom pc_is_weaker

-- Imp ( (And pc_badly_dressed (Not pc_has_weapons)) pc_is_weaker )
-- Not an implication! rather, a Ba(P/Q)

npc_has_loot = NP { pname = "npc_has_loot", idx = 5 }
p_npc_has_loot = Prop.Atom npc_has_loot
l_npc_has_loot = Lit.Atom npc_has_loot

npc_at_door = NP { pname = "npc_at_door", idx = 6 }
p_npc_at_door = Prop.Atom npc_at_door
l_npc_at_door = Lit.Atom npc_at_door

aisha = "Aisha"
boYang = "BoYang"
chinira = "Chinira"

qNP = NP{pname = "q", idx = 2}
sNP = NP{pname = "s", idx = 4}
p = Atom NP{pname = "p", idx = 1}
q = Atom qNP
litq = Lit.Atom qNP
-- Is the light on?
r = Atom NP{pname = "r", idx = 3}
s = Atom sNP
lits = Lit.Atom sNP
notq = Not q
nots = Not s
pthenq = Imp p q

-- REMEMBER plaus 0 to 15; 8/8 is 0.5/0.5

-- We turn on the light, and it either goes ok
-- or fails and we assume nobody turned it on
turnOnLight = EventModelP {
  eventModelId = "turnOnLight",
  -- indistinguishable
  modelEvents = [[(turnOnLightSucc, Plaus16 12), (turnOnLightFail, Plaus16 4)]]
  -- distinguishable
  -- modelEvents = [[(turnOnLightSucc, 12)], [(turnOnLightFail, 4)]]
  }
turnOnLightSucc = EventP {
  eventId = "turnOnLightSucc",
  precond = notq,
  postcond = ([SetL qNP], S.empty),
  model = turnOnLight
}
turnOnLightFail = EventP {
  eventId = "turnOnLightFail",
  precond = T,
  postcond = ([], S.empty),
  model = turnOnLight
}


-- TODO credibility: assign a plausibility to a statement
-- issued by a speaker, but taking also into account the 


sayingSomething = EventModelP {
  eventModelId = "sayingSomething",
  -- epistemic is (almost always) indistinguishable
  modelEvents = 
    [[(sayingSucc, Plaus16 12), (sayingFail, Plaus16 4)]]
  }
sayingSucc = EventP {
  eventId = "sayingSucc",
  precond = T,
  postcond = ([], S.empty),
  model = turnOnLight
}
sayingFail = EventP {
  eventId = "sayingFail",
  precond = T,
  postcond = ([], S.empty),
  model = turnOnLight
}




{--
testExpandOr :: Test
testExpandOr =
  let
    expected = ""
    input = minBelSet belSet

    testOr = OrNode {
      orBelBase = belSet
    }

    expanded = expandOr aisha testOr [turnOnLight]
    
    belSet :: TestBeliefBase 
    belSet = [(S.fromList [p, notq], 1),
              (S.fromList [r], 2),
              (S.fromList [pthenq], 3)]
  in
    TestCase $ assertEqual ("") expected (show expanded)
--}

switchNP = NP{pname = "s", idx = 1}
topNP = NP{pname = "t", idx = 2}
lightNP = NP{pname = "l", idx = 3}
bulbNP = NP{pname = "b", idx = 4}
unharmedNP = NP{pname = "u", idx = 5}

topStairsLit = Lit.Atom topNP
downStairsLit = Lit.Not topStairsLit
lightOnLit = Lit.Atom lightNP
lightOffLit = Lit.Not lightOnLit
bulbOkLit = Lit.Atom bulbNP
bulbNokLit = Lit.Not bulbOkLit
switchOnLit = Lit.Atom switchNP
switchOffLit = Lit.Not switchOnLit
unharmedLit = Lit.Atom unharmedNP
harmedLit = Lit.Not unharmedLit

topStairsCdl :: TestCDLFormula
topStairsCdl = WAI.lit2Cdl topStairsLit
downStairsCdl :: TestCDLFormula
downStairsCdl = WAI.lit2Cdl downStairsLit
lightOnCdl :: TestCDLFormula
lightOnCdl = WAI.lit2Cdl lightOnLit
lightOffCdl :: TestCDLFormula
lightOffCdl = WAI.lit2Cdl lightOffLit
bulbOkCdl :: TestCDLFormula
bulbOkCdl = WAI.lit2Cdl bulbOkLit
bulbNokCdl :: TestCDLFormula
bulbNokCdl = WAI.lit2Cdl bulbNokLit
switchOnCdl :: TestCDLFormula
switchOnCdl = WAI.lit2Cdl switchOnLit
switchOffCdl :: TestCDLFormula
switchOffCdl = WAI.lit2Cdl switchOffLit
unharmedCdl :: TestCDLFormula
unharmedCdl = WAI.lit2Cdl unharmedLit
harmedCdl :: TestCDLFormula
harmedCdl = WAI.lit2Cdl harmedLit

-- type CDLFormula = CDLFormulaP NumProp Float String String

flick :: TestEventModel
flick = EventModelP {
  eventModelId = "flick",
  modelEvents = [[(flickOk, Plaus16 8)], [(flickNok, Plaus16 8)]]
  -- DISTINGUISHABLE
}
flickOk = EventP {
  eventId = "flickOk",
  precond = And (And switchOffCdl bulbOkCdl) topStairsCdl,
  postcond = ([SetL lightNP, SetL switchNP], S.empty),
  model = flick
}
flickNok = EventP {
  eventId = "flickNok",
  precond = And (Or switchOnCdl bulbNokCdl) topStairsCdl,
  postcond = ([UnsetL lightNP, FlipL switchNP], S.empty),
  model = flick
}
  
desc :: TestEventModel
desc = EventModelP {
  eventModelId = "desc",
  modelEvents = [[(descNok, Plaus16 12), (descOk, Plaus16 4)]]
  -- NON DISTINGUISHABLE
  -- An effort should be made to obtain a succinct model
  -- modelEvents = M.singleton aisha [[(descNok, Plaus16 12)], [(descOk, Plaus16 4)]]
}

descOk = EventP {
  eventId = "descOk",
  precond = topStairsCdl,
  postcond = ([UnsetL topNP], S.empty),
  model = flick
}
descNok = EventP {
  eventId = "descNok",
  precond = And lightOffCdl topStairsCdl,
  postcond = ([UnsetL topNP, UnsetL unharmedNP], S.empty),
  model = flick
}

testSimplify :: Test
testSimplify = 
  let
    complexF = Or F (And T (downStairsCdl))
    expected = ""
    in
      TestCase $ assertEqual ("") expected (show $ simplify complexF)




-- FIXME Another idea: preferences on certain independent sentences:
-- AND (p,q,r) --> NOT AND (p,q,r)
testBases :: Test
testBases =
  let
    expected = ""
    belBase :: TestBeliefBase 
    belBase = [(S.fromList [unharmedCdl], Plaus16 1),
               (S.fromList [topStairsCdl, lightOffCdl], Plaus16 2),
               (S.fromList [switchOffCdl], Plaus16 3),
               (S.fromList [bulbOkCdl], Plaus16 5),
               (S.fromList [switchOnCdl, topStairsCdl], Plaus16 7)]
    (know, prioBels) = kandb belBase

    isPossible act = (flip checkPrecond $ act) . S.toList . fst
    evts = [flickOk, flickNok, descOk, descNok]
    arePossible = L.map (($) isPossible) evts
    possibles = L.map ((flip L.map) (sos prioBels)) arePossible

    kandb base = case L.partition ((== maxP) . snd) base of
      ([], pBs) -> (S.empty, pBs)
      ([(k, maxP)], pBs) -> (k, pBs)
    negSAnd = Lit.negate . (L.foldl1' And) . S.toList
    sos :: TestBeliefBase -> TestBeliefBase
    sos (hbase:[]) = [hbase]
    sos ((hbase1,p1):(hbase2,p2):htail) = (neg:rest)
      where
      -- FIXME Change plausibilities into straight probabilities
      -- plausToProb
        neg = (S.insert (negSAnd hbase2) hbase1, plausToProb p2)
        rest = sos ((S.union hbase1 hbase2, p2):htail)
  in
    TestCase $ assertEqual ("Bases> ") expected (show $ (
      prioBels,
      sos prioBels,
      possibles
    ))








testSoSFromBase1 :: Test
testSoSFromBase1 =
  let
    expected = ""
    belBase :: TestBeliefBase 
    belBase = [(S.fromList [unharmedCdl], Plaus16 1),
               (S.fromList [topStairsCdl, lightOffCdl], Plaus16 2),
               (S.fromList [switchOffCdl], Plaus16 3),
               (S.fromList [bulbOkCdl], Plaus16 5),
               (S.fromList [And switchOnCdl topStairsCdl], Plaus16 7)]
  in
    TestCase $ assertEqual ("SoS> ") expected (show $ BR.getSoS belBase)

testSoSFromBase2 :: Test
testSoSFromBase2 =
  let
    expected = ""
    belBase :: TestBeliefBase 
    belBase = [(S.fromList [topStairsCdl, lightOffCdl, switchOffCdl, unharmedCdl], Plaus16 1),
               (S.fromList [bulbOkCdl], Plaus16 3)]
  in
    TestCase $ assertEqual ("SoS> ") expected (show $ BR.getSoS belBase)
    
testLimitSoS1 :: Test
testLimitSoS1 =
  let
    expected = ""
    belBase :: TestBeliefBase 
    belBase = [(S.fromList [topStairsCdl, lightOffCdl, switchOffCdl, unharmedCdl], Plaus16 1),
                (S.fromList [bulbOkCdl], Plaus16 3)]
  in
    TestCase $ assertEqual ("SoS> ") expected (show $ BR.limitSoS [precond flickOk] belBase)
    
testLimitSoS2 :: Test
testLimitSoS2 =
  let
    expected = ""
    belBase :: TestBeliefBase 
    belBase = [(S.fromList [topStairsCdl, lightOffCdl, switchOffCdl, unharmedCdl], Plaus16 1),
                (S.fromList [bulbOkCdl], Plaus16 3)]
  in
    TestCase $ assertEqual ("SoS> ") expected (show $ BR.limitSoS [precond flickNok] belBase)

testSimplifyA :: Test
testSimplifyA =
  let
    expected = ""
    precondFlickOk = And (And switchOffCdl bulbOkCdl) topStairsCdl
    safeLyr = And topStairsCdl $ And lightOffCdl $ And switchOffCdl unharmedCdl
    nextLyr = bulbOkCdl
    (transformed, _) = runState (propify (And precondFlickOk safeLyr)) (0, M.empty)
    simplified :: S.Set (S.Set (Lit.LFormula WAI.ModNumProp))
    simplified = Prop.simpcnf id (Prop.psimplify transformed)
    output = [
      "Is inconsistent: " ++ show (not (isConsistent (S.fromList [precondFlickOk, safeLyr]) nextLyr)),
      "Is implied: " ++ show (isImplied (S.fromList [precondFlickOk, safeLyr]) nextLyr),
      show simplified
      ]
    in
      TestCase $ assertEqual ("Test for layer update A") expected (show output)
    
testSimplifyB :: Test
testSimplifyB =
  let
    expected = ""
    precondFlickNok = And (Or switchOnCdl bulbNokCdl) topStairsCdl
    safeLyr = And topStairsCdl $ And lightOffCdl $ And switchOffCdl unharmedCdl
    nextLyr = bulbOkCdl
    (transformed, _) = runState (propify (And precondFlickNok safeLyr)) (0, M.empty)
    simplified :: S.Set (S.Set (Lit.LFormula WAI.ModNumProp))
    simplified = Prop.simpcnf id (Prop.psimplify transformed)
    output = [
      "Is inconsistent: " ++ show (not (isConsistent (S.fromList [precondFlickNok, safeLyr]) nextLyr)),
      "Is implied: " ++ show (isImplied (S.fromList [precondFlickNok, safeLyr]) nextLyr),
      show simplified
      ]
    in
      TestCase $ assertEqual ("Test for layer update B") expected (show output)
              
testSimplifyC :: Test
testSimplifyC =
  let
    expected = ""
    precondDescOk = topStairsCdl
    safeLyr = And topStairsCdl $ And lightOffCdl $ And switchOffCdl unharmedCdl
    nextLyr = bulbOkCdl
    (transformed, _) = runState (propify (And precondDescOk safeLyr)) (0, M.empty)
    simplified :: S.Set (S.Set (Lit.LFormula WAI.ModNumProp))
    simplified = Prop.simpcnf id (Prop.psimplify transformed)
    output = [
      "Is inconsistent: " ++ show (not (isConsistent (S.fromList [precondDescOk, safeLyr]) nextLyr)),
      "Is implied: " ++ show (isImplied (S.fromList [precondDescOk, safeLyr]) nextLyr),
      show simplified
      ]
    in
      TestCase $ assertEqual ("Test for layer update C") expected (show output)
          
testSimplifyD :: Test
testSimplifyD =
  let
    expected = ""
    precondDescNok = And lightOffCdl topStairsCdl
    safeLyr = And topStairsCdl $ And lightOffCdl $ And switchOffCdl unharmedCdl
    nextLyr = bulbOkCdl
    (transformed, _) = runState (propify (And precondDescNok safeLyr)) (0, M.empty)
    simplified :: S.Set (S.Set (Lit.LFormula WAI.ModNumProp))
    simplified = Prop.simpcnf id (Prop.psimplify transformed)
    output = [
      "Is inconsistent: " ++ show (not (isConsistent (S.fromList [precondDescNok, safeLyr]) nextLyr)),
      "Is implied: " ++ show (isImplied (S.fromList [precondDescNok, safeLyr]) nextLyr),
      show simplified
      ]
    in
      TestCase $ assertEqual ("Test for layer update D") expected (show output)

testSimplePlanner1 :: Test
testSimplePlanner1 =
  let
    expected = ""
    precondDescNok = And lightOffCdl topStairsCdl
    safeLyr = And topStairsCdl $ And lightOffCdl $ And switchOffCdl unharmedCdl
    nextLyr = bulbOkCdl
    (transformed, _) = runState (propify (And precondDescNok safeLyr)) (0, M.empty)
    simplified :: S.Set (S.Set (Lit.LFormula WAI.ModNumProp))
    simplified = Prop.simpcnf id (Prop.psimplify transformed)
    output = [
      "Is inconsistent: " ++ show (not (isConsistent (S.fromList [precondDescNok, safeLyr]) nextLyr)),
      "Is implied: " ++ show (isImplied (S.fromList [precondDescNok, safeLyr]) nextLyr),
      show simplified
      ]
    in
      TestCase $ assertEqual ("Test for layer update D") expected (show output)

{-- FIXME This test needs to be done:
* very different preconditions
* epistemic updates

testUpdate :: Test
testUpdate =
  let
    pOrBelQ = Or 
      p
      (Bel aisha q)
    rOrLightOnS = Or
      r
      (Act (model turnOnLightSucc) turnOnLightSucc s)
    belSet :: TestBeliefBase
    belSet = [(S.fromList [pOrBelQ, q, nots, rOrLightOnS], Plaus16 0)]
    newBelSet :: TestBeliefBase
    newBelSet = ??
    in
      TestCase $ assertEqual ("Test for belief base update") (show belSet) (show newBelSet)
--}

{--
testEpisPlan :: Test
testEpisPlan =
  let
    expected = ""
    input = minBelSet belSet

    testOr = OrNode {
      orBelBase = belSet
    }

    expanded = expandOr aisha testOr [flick, desc]
    
    belSet :: TestBeliefBase 
    belSet = [(S.fromList [topStairsCdl, lightOffCdl, switchOffCdl, unharmedCdl], 1),
              (S.fromList [bulbOkCdl], 3)]
  in
    TestCase $ assertEqual ("") expected (show expanded)

testEpisPlan2 :: Test
testEpisPlan2 =
  let
    expected = ""
    input = minBelSet belSet

    testOr = OrNode {
      orBelBase = belSet
    }

    expanded = expandOr aisha testOr [flick, desc]
    
    belSet :: TestBeliefBase 
    belSet = [(S.fromList [topStairsCdl, lightOffCdl, switchOffCdl, unharmedCdl], 1),
              (S.fromList [bulbOkCdl], 3)]

    afterFlick = (head . alts) (expanded M.! "flick")
    -- afterFlick = (orBelBase . head . alts) (expanded M.! "flick")

    expandAfterFlick = expandOr aisha afterFlick [flick, desc]
  in
    TestCase $ assertEqual ("") expected (show expandAfterFlick)
--}

testApplyEffect1 :: Test
testApplyEffect1 =
  let
    belSet :: TestBeliefBase 
    belSet = [(S.fromList [topStairsCdl, lightOffCdl, switchOffCdl, unharmedCdl], 1),
              (S.fromList [bulbOkCdl], 3)]

    (postLitFlickOk, _) = postcond flickOk
    precondFlickOk = precond flickOk
    (postLitFlickNok, _) = postcond flickNok
    precondFlickNok = precond flickNok
    (postLitDescOk, _) = postcond descOk
    precondDescOk = precond descOk
    (postLitDescNok, _) = postcond descNok
    precondDescNok = precond descNok

  in
    TestCase $ assertEqual ("Test apply action effect 1") ""
      (show (belSet,
             precondFlickOk,
             postLitFlickOk,
             applySomeLitEffect precondFlickOk postLitFlickOk belSet,
             precondFlickNok,
             postLitFlickNok,
             applySomeLitEffect precondFlickNok postLitFlickNok belSet))


{--

testTrickGuards :: Test
testTrickGuards = 
  let
    guard1
    guard2
    player
    player : I can handle a single guard
    guard1 : player is weaker
           : player does not have treasure
           : if player knows then they might tell
           : if player is killed they don't tell
    guard2 : player is weaker
           : player does not have treasure
           : if player knows then they might tell
           : if player is killed they don't tell
    player says: if player is killed people may investigate
    player says: if you capture the noble and give me half I won't tell
  in
    TestCase $ assertEqual ("Test trick guards") expected (show input)

--}

testDummyIsImplied :: Test
testDummyIsImplied =
  let
    expected = ""
    input = ("Is T implied from []: " ++ show tImplied, "Is F implied from []: " ++ show fImplied)
    none :: S.Set TestCDLFormula
    none = S.empty
    tImplied = isImplied none T
    fImplied = isImplied none F
      in
    TestCase $ assertEqual ("Test for dummy cases in isImplied") expected (show input)

testDummyIsConsistent :: Test
testDummyIsConsistent =
  let
    expected = ""
    belSet = [topStairsCdl, lightOffCdl, switchOffCdl, unharmedCdl]
    input = "Is belSet consistent from []: " ++ show implied
    none :: S.Set TestCDLFormula
    none = S.empty
    implied = L.map (isConsistent none) belSet
    in
      TestCase $ assertEqual ("Test for dummy cases in isConsistent") expected input
  
testSubsumption :: Test
testSubsumption =
  let
    expected = ""
    belSet = [topStairsCdl, lightOffCdl, switchOffCdl, unharmedCdl]
    lits1 = S.fromList [topStairsCdl, lightOffCdl, switchOffCdl, unharmedCdl]
    lits2 = S.fromList [topStairsCdl, lightOffCdl]
    lits3 = S.fromList [topStairsCdl]
    lits4 = S.fromList [switchOffCdl, unharmedCdl]
    input = show (subsumes lits1 lits3,
                  subsumes lits1 lits2,
                  subsumes lits1 lits4,
                  subsumes lits4 lits3,
                  subsumes lits3 lits2)
    in
      TestCase $ assertEqual ("Test for subsumption") expected input

testOrNodes :: Test
testOrNodes =
  let
    expected = ""
    belSet :: TestBeliefBase 
    belSet = [(S.fromList [topStairsCdl, lightOffCdl, switchOffCdl, unharmedCdl], 1),
              (S.fromList [bulbOkCdl], 3)]
    startOr = OrNode {beliefBaseUnion = belSet, altActions = M.empty, epistemicState = M.empty}
    input = show $ expandOr startOr [flick, desc]
    in
      TestCase $ assertEqual ("Test OR node creation") expected input




      
main :: IO ()
main = runTestTT (TestList  [
                             TestLabel "Test for SoS expansion 1" testSoSFromBase1,
                             TestLabel "Test for SoS expansion 2" testSoSFromBase2,
                             -- TestLabel "Test SoS limiting via precondition 1" testLimitSoS1,
                             -- TestLabel "Test SoS limiting via precondition 2" testLimitSoS2,
                             -- TestLabel "Test for layer update A" testSimplifyA,
                             -- TestLabel "Test for layer update B" testSimplifyB,
                             -- TestLabel "Test for layer update C" testSimplifyC,
                             -- TestLabel "Test for layer update D" testSimplifyD,
                             -- TestLabel "Test for belief base update" testUpdate,
                             -- TestLabel "Test for belief set" testExpandOr,
                             -- TestLabel "Test for simplification" testSimplify,
                             -- TestLabel "Test for epistemic planning" testEpisPlan,
                             -- TestLabel "Test for epistemic planning - 2 steps" testEpisPlan2
                             TestLabel "Test OR node creation" testOrNodes,
                             TestLabel "Test apply action effect 1" testApplyEffect1
                             -- TestLabel "Test for subsumption" testSubsumption,
                             -- TestLabel "Test for SoS generation *again*" testBases
                             -- TestLabel "Test for dummy cases in isImplied" testDummyIsImplied,
                             -- TestLabel "Test for dummy cases in isConsistent" testDummyIsConsistent
                             ]) >>= doCounts
    where
      doCounts counts' = exitWith (if errors counts' /= 0 || failures counts' /= 0 then ExitFailure 1 else ExitSuccess)
