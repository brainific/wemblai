{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ApplicativeDo #-}
{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE MultiParamTypeClasses #-}
-- {-# LANGUAGE ScopedTypeVariables #-}

module DelPlanner
  ( AndNode(..)
  , OrNode(..)
  , applySomeLitEffect
  , orCatLit
  , expandOr
  ) where

import WemblAI as WAI
  ( CDLFormulaP(..)
  , EventModelP(..)
  , EventP(..)
  , LitEffect(..)
  , MentalState(..)
  , NumProp(..)
  , IsDynamic
  , IsDoxastic
  , EventModelType
  , EventType
  , BeliefBase(..)
  , Plaus
  , EpistemicState(..)
  , tableaux
  , mapFst
  , checkPrecond
  , isConsistent
  , isImplied
  , simplify
  , propify
  , propifyAtom
  , propifyEffect
  , propifySet
  , unpropify
  , cnfStateMerge
  , lit2Prop
  , maxP)

import BeliefRevision as BR
  (purify
  ,modNumAt
  ,reverseMap
  ,makeConsistent
  ,negateConj
  ,expandsAndsSet
  )

import Data.Logic.ATP.DefCNF
import Data.Logic.ATP.Prop as Prop
  (PFormula(..)
  ,Prop(..)
  ,simpcnf
  )
import Data.Logic.ATP.Lit as Lit
  (LFormula(..)
  ,Lit(..)
  ,(¬)
  ,negate
  ,foldLiteral'
  ,foldLiteral
  )
import Data.Logic.ATP.Formulas as Formulas
  (IsAtom(..),
   fromBool,
   true,
   false
  )
import Data.Maybe
  (catMaybes
  )
import Data.Set as S
import Data.List as L
import Data.Map as M
import Data.Ord as Ord
import Data.Maybe as Maybe

type Agent = String
type Formula = PFormula Prop
type Literal = LFormula Prop
-- A disjunction of conjunctions
type DNF = Set (Set Literal)
-- A DNF representing a set of states
-- TODO Is this appropriate for a common / knowledge belief?
type PartialState = DNF

-- Contingent Planning as AND/OR Forward Search with Disjunctive Representation

-- set of literals:
-- consistent if no p and not p are included
-- isConsistent :: PartialState -> Bool

-- complete if every p or not p are included
-- state is consistent and complete

-- Don't Plan for the Unexpected: Planning Based on Plausibility Models

-- Set (Set Prop) for "real" propositions?
-- And we need rules about how to change real props;
-- look for revision and expansion just in case

data AndNode atom plaus agent evtid time = AndNode {
  alts :: [[OrNode atom plaus agent evtid time]]
}
instance (Show agent, Show plaus, Show evtid, Show atom) => 
  Show (AndNode atom plaus agent evtid time) where
    show AndNode{alts = myAlts} = "AND{alts = " ++ show myAlts ++ "}"

data OrNode atom plaus agent evtid time = OrNode {
  beliefBaseUnion :: BeliefBase atom plaus agent evtid,
  epistemicState :: WAI.EpistemicState atom plaus agent evtid,
  altActions :: Map evtid (AndNode atom plaus agent evtid time)
  -- Maybe: Nothing means not explored
  --        Just (M.null) means no action can be taken
}

instance (Show agent, Show plaus, Show evtid, Show atom) => 
  Show (OrNode atom plaus agent evtid time) where
    show OrNode{beliefBaseUnion = myBeliefBaseUnion,
                altActions = myAltActions} =
      "OR{orBelBase=" ++ show myBeliefBaseUnion ++ "; altActions=" ++ show myAltActions ++ "}"

class (Show id, Ord id) => ShOrdId id

applyBelEffect :: c -> b -> a -> a
applyBelEffect = const $ const $ id

{--
applySomeEpiEffect :: (WAI.Plaus plaus, Ord evtid, Ord agent) =>
  CDLFormulaP NumProp plaus agent evtid ->
  [EpiEffect agent atom] -> **OBSERVING AGENT OR ATOM**
  BeliefBase NumProp plaus agent evtid ->
  Maybe (
    BeliefBase NumProp plaus agent evtid,
    BeliefBase NumProp plaus agent evtid)

applySomeCommEffect :: (WAI.Plaus plaus, Ord evtid, Ord agent) =>
  CDLFormulaP NumProp plaus agent evtid ->
  [CommEffect agent atom] -> **OBSERVING AGENT OR ATOM**
  BeliefBase NumProp plaus agent evtid ->
  Maybe (
    BeliefBase NumProp plaus agent evtid,
    BeliefBase NumProp plaus agent evtid)
--}

applySomeLitEffect :: (WAI.Plaus plaus, Ord evtid, Ord agent) =>
  CDLFormulaP NumProp plaus agent evtid ->
  [LitEffect NumProp] ->
  BeliefBase NumProp plaus agent evtid ->
  Maybe (
    BeliefBase NumProp plaus agent evtid,
    BeliefBase NumProp plaus agent evtid)
-- Returns new base, plus evicted beliefs
applySomeLitEffect precond effs bs = case L.inits bs of
  [[]] -> Nothing
  ([] : bases) -> case possibleBases of
      [] -> Nothing
      _ -> Just (removeInconsistent, evicted)
    where
      compliesPrecond = ((flip isConsistent) precond) . (L.foldl1' S.union) . (L.map fst)
      possibleBases = L.takeWhile (compliesPrecond) bases
      maybePossible = case possibleBases of
        [] -> Nothing
        _ -> Just possibleBases
      (consistentCurrent, possibleBasesPlausible) = case L.partition ((== WAI.maxP) . snd) (L.last $ possibleBases) of 
        ([(certainSet, _)], rest) -> 
          -- must be consistent with precond
          (S.insert precond certainSet, rest)
        ([], rest) ->
          (S.singleton precond, rest)
      (propConsistCur, (_, fwdMap)) = propifySet consistentCurrent
      -- > The following lines should provide an updated set of beliefs
      postStateCnf = (applyLitList (L.map propifyEffect effs) . cnfStateMerge) propConsistCur
      certainApplied = S.map (unpropify (reverseMap fwdMap)) (S.map orCatLit postStateCnf)
      preSimplifiedBases = ( (certainApplied, WAI.maxP) : possibleBasesPlausible )
      (removeImplied, _) = L.foldl' (findImplied) ([], S.empty) preSimplifiedBases
        where
          findImplied (layers, current) (base, plaus) = (newLayers, newCurrent)
            where
              baseFilt = S.filter (not . (WAI.isImplied current)) base
              newCurrent = S.union current baseFilt
              newLayers = case S.null baseFilt of
                True -> layers
                False -> layers ++ [(baseFilt, plaus)]
      (removeInconsistent, _, evicted) = L.foldl' (findInconsistent) ([], S.empty, []) removeImplied
        where
          findInconsistent (layers, current, out) (base, plaus) = (newLayers, newCurrent, newOut)
            where
              (cons, incons) = S.partition (WAI.isConsistent current) base
              newCurrent = S.union current cons
              newLayers = case S.null cons of
                True -> layers
                False -> layers ++ [(cons, plaus)]
              newOut = case S.null incons of
                True -> out
                False -> out ++ [(incons, plaus)]


-- EPISTEMIC EFFECTS
-- The fact that someone takes an action demonstrates:
-- * that they're aware of their preconditions
-- * that they're aware of the epistemic postconditions
-- 
-- The dact that someone take a communication action demonstrates:





-- We need to separate *update*, as in a dynamic world, from 
-- *revision*, where new information comes about a static world
-- Note that a flip effect on a base that does not include a literal
-- means it is basically unaffected

-- setPrecond precond ((safe, 1):rest) = ((precond, 1):(safe, 1):rest)
-- setPrecond precond base = ((precond, 1):base)

-- FIXME check if we need to make it consistent
-- fixPrecond precond = BR.makeConsistent . BR.purify . (L.map (mapFst BR.expandsAndsSet)) .
--                  (setPrecond (S.singleton precond))

-- Effects are specified using unpropified atoms. So how to
-- change atoms?

applyLit :: (Formulas.IsAtom atom) =>
            LitEffect atom ->
            S.Set (S.Set (Lit.LFormula atom)) ->
            S.Set (S.Set (Lit.LFormula atom))
applyLit eff cnfLayer =
  case eff of
  SetL at   ->
    let (withAt, withoutAt) = S.partition (S.foldr' ((||) . isOfAtom at) False) cnfLayer
        withAtSet = if (S.null withAt) then S.singleton (S.singleton (Lit.Atom at))
          else S.map (setLit (Lit.Atom at)) withAt
    in
      S.union withAtSet withoutAt
  UnsetL at ->
    let (withAt, withoutAt) = S.partition (S.foldr' ((||) . isOfAtom at) False) cnfLayer
        withAtSet = if (S.null withAt) then S.singleton (S.singleton (Lit.Not $ Lit.Atom at))
          else S.map (setLit (Lit.Not $ Lit.Atom at)) withAt
    in
      S.union withAtSet withoutAt
    -- S.map (setLit (Lit.Not $ Lit.Atom at)) cnfLayer
  FlipL at  ->
    let (withAt, withoutAt) = S.partition (S.foldr' ((||) . isOfAtom at) False) cnfLayer
        withAtSet = if (S.null withAt) then withAt
          else S.map (flipLit at) withAt
    in
      S.union withAtSet withoutAt
    -- S.map (flipLit at) cnfLayer
applyLitSet  :: (Formulas.IsAtom atom) =>
                S.Set (LitEffect atom) ->
                S.Set (S.Set (Lit.LFormula atom)) ->
                S.Set (S.Set (Lit.LFormula atom))
applyLitSet = flip (S.foldr' (applyLit))

applyLitList  :: (Formulas.IsAtom atom) =>
                 [LitEffect atom] ->
                 S.Set (S.Set (Lit.LFormula atom)) ->
                 S.Set (S.Set (Lit.LFormula atom))
applyLitList = flip (L.foldl' (flip applyLit))

setLit l = (S.insert l) . (S.delete (Lit.negate l))
flipLit at = S.map (\lit -> if isOfAtom at lit then Lit.negate lit else lit)

isOfAtom :: (Formulas.IsAtom atom) =>
            atom ->
            Lit.LFormula atom ->
            Bool
isOfAtom at (Lit.Atom at2)
  | at == at2 = True
  | at /= at2 = False
isOfAtom at (Lit.Not (Lit.Atom at2))
  | at == at2 = True
  | at /= at2 = False
isOfAtom _ _ = False

orCatLit :: (IsAtom atom) =>
            Set (LFormula atom) ->
            PFormula atom
orCatLit someLits
  | S.null someLits = Prop.T
  | otherwise       = L.foldl1' (Prop.Or) (L.map (WAI.lit2Prop) (S.toList someLits))


orCatLit2 :: (Formulas.IsAtom atom) =>
          S.Set (Lit.LFormula atom) ->
          Prop.PFormula atom
orCatLit2 litSet = combine $ S.toList litSet
  where
    lToP Lit.T = Prop.T
    lToP Lit.F = Prop.F
    lToP (Lit.Atom a) = Prop.Atom a
    lToP (Lit.Not l) = Lit.negate $ lToP l
    combine [] = Prop.T
    combine [one] = lToP one
    combine (one:two:rest) =
      L.foldl' (\r a -> Prop.Or r (lToP a)) (Prop.Or (lToP one) (lToP two)) rest
          


type NPIBBase agent evtid = BeliefBase NumProp Integer agent evtid

-- Plausibility-ordered possible results
-- Each sibling of the AND node holds an alternative
-- Alternatives are a list of distinguished lists of undistinguishable states
-- FIXME Incorporate as many beliefs as possible without
-- contradiction
-- For the moment, just take the first layer
maxBelSupport = fst . head

belief2Cdl :: (agent, CDLFormulaP atom plaus agent evtid) ->
              CDLFormulaP atom plaus agent evtid
belief2Cdl = uncurry WAI.Bel

-- TODO Currently action postconditions do not overlap with Act statements

-- OR node --> different actions leading to AND nodes
-- AND node --> one action's outcomes leading to OR nodes


{--
-- Expands an OR node with suitable next nodes
-- built as possible actions (that expand to actual
-- indistinguishable actions in AND nodes)
-- The next nodes are returned as paths 
-- Note that the current AND branches are deleted --}
expandOr :: (Show plaus, Show agent, Plaus plaus, Ord agent, Show evtid, Ord evtid) =>
            OrNode NumProp plaus agent evtid time ->
            [EventModelP NumProp plaus agent evtid] ->
            OrNode NumProp plaus agent evtid time
expandOr OrNode{beliefBaseUnion = state, epistemicState = epiState} eventMList = OrNode {
  beliefBaseUnion = state,
  epistemicState = newEpiState,
  altActions = M.map (expandAnd state) actionMap}
  where
    actionMap = M.fromList $ L.map (\ev -> (eventModelId ev, modelEvents ev)) eventMList
    -- ALLOW DIFFERENT EPISTEMIC EFFECTS
    newEpiState = epiState
    expandAnd state evtAlts = AndNode{
      -- TODO catMaybes, plus remove empty alternatives
      -- add plaus back when alternative is mapped
      alts = L.map (L.map wrap . (L.filter (not . L.null)) . L.map ((buildBelBase state) . fst)) evtAlts
    }
    wrap bbase = OrNode {
      beliefBaseUnion = bbase,
      epistemicState = newEpiState,
      altActions = M.empty
    }
    -- list of lists, for epistemically indistinguishable alternatives
    buildBelBase state evt = case applySomeLitEffect (precond evt) effs state of 
      Nothing -> []
      Just (newBase, evicted) -> newBase
      where
        epis = snd (postcond evt)
        -- Set (agent, CDLFormulaP atom plaus agent evtid)
        effs = fst (postcond evt)
          -- what to do with evicted
    -- remove all events that are incompatible with precond, and apply update with precond
    -- apply revision with postcond

{--
        possibleEffects = 
          ((L.map (consolidateInEvt . L.map (WAI.mapFst (catSWithAnd . (buildEffect state))))) . _)
        apply state possibleEffects = redoPlaus $ revise $ effectStates ++ state
          where
            effectStates = L.map (WAI.mapFst (S.singleton . WAI.simplify)) possibleEffects
        redoPlaus = id -- FIXME rework plausibilities according to order?
        revise (new:old) = new:(L.map (mapFst $ S.filter (WAI.isConsistent (fst new))) old)
        catSWithAnd = S.foldl' (WAI.And) (WAI.T)
        catLWithOr = L.foldl' (WAI.Or) (WAI.F)
        consolidateInEvt :: _
        consolidateInEvt fs = (catLWithOr formulas, pl)
          where
            (formulas, pl:_) = unzip fs
        groupByPlaus = L.groupBy (\x y -> snd x == snd y)
--}
--}



{--
Plan(;P)
1 Let T be the -planning tree only consisting of root(T) labelled by the
initial state of P.
2 Repeatedly apply the tree expansion rule of P to T until no more rules apply
satisfying condition B.
3 If root(T) is -solved, return (root(T)), otherwise return fail.

Denition 27 (Tree Expansion Rule). Let T be a planning tree for a planning problem
P = (M0;A; g). The tree expansion rule is dened as follows. Pick an or-node n
in T and an event model E 2 A applicable in M(n) with the proviso that E does not
label any existing outgoing edges from n. Then:
1. Add a new and-node m to T with M(m) =M(n) 
 E, and add an edge (n;m)
with E(n;m) = E.
2. For each information cell M0 in M(m), add an or-node m0 with M(m0) =M0
and add the edge (m;m0).

Note that the
expansion rule applies only to or-nodes, and that an applicable event model can only
be used once at each node.

Considering single-agent planning a two-player game, a useful analogy for planning
trees are game trees. At an or-node n, the agent gets to pick any applicable action E
it pleases, winning if it ever reaches an information model in which the goal formula
holds (see the denition of solved nodes further below). At an and-node m, the
environment responds by picking one of the information cells of M(m)|which of the
distinguishable outcomes is realised when performing the action.

Without restrictions on the tree expansion rule, even very simple planning problems
might be infinitely expanded (e.g. by repeatedly choosing a no-op action). Finiteness
of trees (and therefore termination) is ensured by the following blocking condition.

B The tree expansion rule may not be applied to an or-node n for which there exists
an ancestor or-node m with M(m) M(n).

Lemma 28 (Termination). Any planning tree built by repeated application of the tree
expansion rule under condition B is nite.
--}

-- A General Multi-agent Epistemic Planner Based on Higher-order Belief Change

-- Ka (v Phi) ^ La Phi =
-- Ka (phi1 v ... v phin) ^ La (phi1) ^ ... ^ La (phin) =
-- Ka (phi1 v ... v phin) ^ ¬Ka(¬phi1) ^ ... ^ ¬Ka(¬phin)
-- Phi ^ psi = {phi ^ psi | phi c Phi}
-- Delta_a {T} <=> T
-- Ka psi ^ La Phi <=> Delta a ({psi} U {Phi ^ psi})
-- Delta a Phi1 ^ Delta a Phi2 <=> Delta a [(Phi1 ^ (V Phi2)) U (Phi2 ^ (V Phi1))]
data Delta = Delta Agent (Set Formula)
-- Conjunction of literals
type CDFProp = Set Literal
-- Disjunctive set of CDF, conjunctive set of Delta
data ACDF = Set (CDFProp, Set Delta)

-- ACDF term [phi0 ^ ^(a e B)(Delta a Phi)] is satisfiable wrt DNF [gamma] if:
-- 1. phi0 ^ gamma
-- 2. Va, Phi is not empty
-- 3. Va, Vphi e Phi, phi is satisfiable by gamma

-- deterministic action: <pre, {cond, cef}>
-- sensing action: <pre, pos+, neg->

-- 