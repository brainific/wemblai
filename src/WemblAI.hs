{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ApplicativeDo #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module WemblAI (
    NumProp(..),
    ModNumProp(..),
    MNPType(..),
    CDLFormula(..),
    CDLFormulaP(..),
    CDLTabTerm(..),
    TabTerm(..),
    WorldLabel,
    BeliefBase(..),
    EpistemicState(..),
    MentalState(..),
    propifyAtom,
    propifyEffect,
    propify,
    propifySet,
    unpropify,
    reverseMap,
    tableaux,
    tableauxAnd,
    tableauxStepTM,
    TableauxBranch(..),
    EventP(..),
    EventModelP(..),
    Event(..),
    EventModel(..),
    LitEffect(..),
    EventId,
    expandL,
    altHst,
    HasNext,
    IsDynamic(..),
    IsDoxastic(..),
    checkPrecond,
    isImplied,
    isConsistent,
    mapFst,
    simplify,
    lit2Cdl,
    subsumptionStep,
    subsumes,
    implyStep,
    lit2Prop,
    cnfStateMerge,
    Plaus,
    Plaus8(..),
    Plaus16(..),
    minP,
    maxP,
    plausToProb
  ) where
  
-- import
import Data.Tree
import Data.Ord
import Data.Set as S
import Data.Array.IArray as IAr
import Data.Maybe
import Data.Functor ( (<$>) )
import Data.List as L
import Data.Map.Strict as M
import Data.Data (Data)
import Data.Typeable (Typeable)
import Data.Logic.ATP.DefCNF as DefCNF (
  defcnfs
 ,NumAtom(..) )
import Data.Logic.ATP.FOL as FOL (
  EqFormula )
import Data.Logic.ATP.Formulas as Formulas (
  fromBool
 ,AtomOf(..)
 ,IsAtom
 ,prettyBool )
import qualified Data.Logic.ATP.Lit as Lit (
  LFormula(..)
 ,IsLiteral(..)
 ,negate )
import qualified Data.Logic.ATP.Prop as Prop (
  -- Prop(P, pname)
  PFormula(..)
 ,IsPropositional(..)
 ,JustPropositional(..)
 ,simpdnf
 ,simpcnf
 ,(∨)
 ,(∧)
 ,(.<=>.)
 ,(.&.)
 ,(.|.)
 ,foldPropositional
 ,BinOp(..) )
import Data.Logic.ATP.PropExamples (Knows(K), prime)
import Data.Logic.ATP.Pretty as Pretty (
  Associativity(InfixL, InfixR, InfixN, InfixA)
 ,Precedence
 ,notPrec
 ,andPrec
 ,orPrec
 ,impPrec
 ,iffPrec
 ,leafPrec
 ,boolPrec
 ,HasFixity(..)
 ,Pretty(..)
 ,Side(..)
 ,testParen
 ,text )
import Text.PrettyPrint.HughesPJClass as HPJC
import Data.Logic.ATP.DP as DP (
      dp,   dpsat,   dptaut
    , dpli, dplisat, dplitaut
    , dpll, {--dpllsat,--} dplltaut
    , dplb, dplbsat, dplbtaut
    , testDP
    )
import Control.Monad.State.Strict
import Control.Applicative

import Debug.Trace

-- There might be a StatePart that covers the whole set of worlds, mainly,
-- Set.singleton T
-- Conversely, there's a StatePart that covers no worlds, mainly,
-- Set.singleton F
-- If all Props are atoms, and all atoms are convered, then this represents
-- a single world - is this really true?
-- Try Potassco as world generator

-- NOTES
-- Meson is for FO formulas;
-- DPLL is the standard for SAT solvers; CDCL(Conflict Drive Clause Learning) is now
-- SotA

-- We need to use NumProp since Prop does not instances Knows Integer
-- TODO How to ensure that a set of prespecified atomic propositions uses a
-- consistent numbering???

{-- TODO Future improvement
-- This procedure depends on two features:
-- * NumAtom ai, which provides a "big enough" number to identify
--   new props, and
-- * A state transf function ((pf, Map pf pf, Integer) -> (pf, Map pf pf, Integer))
-- In our case, we're using a state monad for exactly this reason. Let's try to unify
-- criteria later on.

-- FIXME adaptation of dpllsat but removing dependencies
wsat :: (Prop.JustPropositional pf, Ord pf) => pf -> Bool
wsat = dpll . defcnfs

--}

-- BeliefBase has plausibilities:
-- 0: certain (knowledge)
-- 1: nearly certain (93.75%) to
-- 7: barely certain (56.25%)
-- If we apply an outcome with probability 0-16
-- p > p': 0.75
-- Given that p, q > q': 0.66
-- pq  : most probable    --> 0.50  (8)
-- pq' : not so probable  --> 0.25  (4)
-- p'q': less probable    --> 0.125 (2)
-- p'q : less probable    --> 0.125 (2)

class (Num plaus, Ord plaus) => Plaus plaus where
  -- Min plaus equals perfect uncertainty (0.5)
  minP :: plaus
  -- Max plaus equals perfect certainty (0 or 1)
  maxP :: plaus
  plausToProb :: plaus -> plaus
  -- combine :: [plaus] -> plaus
  -- plausToProb plaus = 2*minP - plaus

-- TODO We need some way of combining
-- plausibilities of a belief base with probabilities
-- of a set of actions
newtype Plaus8 = Plaus8 Int deriving (Eq, Show, Ord, Num)
instance Plaus Plaus8 where
  minP = Plaus8 4
  maxP = Plaus8 0
  plausToProb plaus = 2*minP - plaus
--  combine = combineInt 8
newtype Plaus16 = Plaus16 Int deriving (Eq, Show, Ord, Num)
instance Plaus Plaus16 where
  minP = Plaus16 8
  maxP = Plaus16 0
  plausToProb plaus = 2*minP - plaus
--  combine = combineInt 16

{--
combineInt :: [Integer] -> Integer
combineInt max intList = (floor ((intSum / listLen) - maxP)) + maxP
  where
    intSum = fromIntegral (L.foldl' (+) 0 intList)
    listLen = fromIntegral $ L.length intList
--}

data MNPType = Propos | Modal
  deriving (Eq, Show, Ord)
data ModNumProp = MNP {mpname :: String, mptype :: MNPType, midx :: Integer} deriving (Eq, Ord)
-- TODO maybe carry modal formula directly here instead of on a map?
instance Show ModNumProp where
    show MNP{mpname = s, mptype = mpt, midx = i} = s ++ " <" ++ show mpt ++ "> p" ++ show i
instance IsAtom ModNumProp
instance Pretty ModNumProp where
    pPrint = text . show
instance HasFixity ModNumProp where
    precedence mnp = Pretty.leafPrec
-- TODO define a new MNPType?
instance DefCNF.NumAtom ModNumProp where
    ma n = MNP { mpname = "<unnamed> p_" ++ show n, mptype = Propos, midx = n }
    ai = midx

data NumProp = NP {pname :: String, idx :: Integer} deriving (Eq, Ord)
instance Show NumProp where
    show NP{pname = s, idx = i} = s ++ " p" ++ show i
instance IsAtom NumProp
instance Pretty NumProp where
    pPrint = text . show
instance HasFixity NumProp where
    precedence np = Pretty.leafPrec
instance DefCNF.NumAtom NumProp where
    ma n = NP { pname = "<unnamed> p_" ++ show n, idx = n }
    ai = idx

{--
"Seeing is Believing: Formalising False-Belief Tasks in Dynamic Epistemic Logic"
Postconditions
are conjunctions of propositional literals, i.e., conjunctions of atomic propositions
and their negations (including > and ?).

"Dynamic Epistemic Logic with Assignment"
* test
* assignment
* learning after some local action; check Baltag with postconditions or Bolander
--}

data CDLFormulaP atom plaus agent evtid
    = F
    | T
    | Atom atom
    | Not (CDLFormulaP atom plaus agent evtid)
    | And (CDLFormulaP atom plaus agent evtid)
          (CDLFormulaP atom plaus agent evtid)
    | Or (CDLFormulaP atom plaus agent evtid)
         (CDLFormulaP atom plaus agent evtid)
    | Imp (CDLFormulaP atom plaus agent evtid)
          (CDLFormulaP atom plaus agent evtid)
    | Iff (CDLFormulaP atom plaus agent evtid)
          (CDLFormulaP atom plaus agent evtid)
    -- Watch out!!! The dynamic modality uses a POINTED event model, so
    -- we need to specify in the formula which event actually happened
    | Act (EventModelP atom plaus agent evtid)
          (EventP atom plaus agent evtid)
          (CDLFormulaP atom plaus agent evtid)
    -- TODO Unify with event models!!!
    | Bel agent (CDLFormulaP atom plaus agent evtid)
    | BelQ agent plaus (CDLFormulaP atom plaus agent evtid)
-- ?????
--    | Belf agent (CDLFormulaP atom plaus agent evtid) (CDLFormulaP atom plaus agent evtid)
--    | "come to believe phi upon learning psi"
    deriving (Eq, Ord, Typeable, Show)

-- FIXME Add a flag that tells what kinds (classical, doxastic or dynamic)
-- of formulae are used within the arguments
-- Replacing And/Or with *sets* or *lists* of arguments may be a good idea
-- (eg when transforming into CNF/DNF) 

modName :: String
modName = "modal_"
modInt :: Integer 
modInt  = 0

lit2Cdl :: Lit.LFormula atom -> CDLFormulaP atom plaus agent evtid
lit2Cdl Lit.T = T
lit2Cdl Lit.F = F
lit2Cdl (Lit.Atom at) = Atom at
lit2Cdl (Lit.Not lit) = Not $ lit2Cdl lit

type CDLFormula = CDLFormulaP NumProp Integer String String
type CDLTabTerm = TabTerm WorldLabel NumProp Integer String String

-- FIXME GENERALIZE!!! CDLFormulaP atom plaus agent evtid
type TransfMState plaus agent evtid = (Integer, M.Map (CDLFormulaP NumProp plaus agent evtid) ModNumProp)

-- TODO Open the way for more complex effects:
-- Either Forget atom, or
-- Set atom [the rest of vars] -> Bool (set to true or false) or even "Var val"
-- for properties beyond boolean
propifyEffect :: LitEffect NumProp ->
                 LitEffect ModNumProp 
propifyEffect (SetL atom) = SetL $ propifyAtom atom
propifyEffect (UnsetL atom) = UnsetL $ propifyAtom atom
propifyEffect (FlipL atom) = FlipL $ propifyAtom atom
propifyEffect (ForgetL atom) = ForgetL $ propifyAtom atom

propifyAtom :: NumProp ->
               ModNumProp
propifyAtom NP{pname = pn, idx = idxInt} =
  MNP{mpname = pn, mptype = Propos, midx = idxInt}

-- TODO check if fold solves this
propify :: (Ord agent, Ord evtid, Ord plaus) =>
           CDLFormulaP NumProp plaus agent evtid ->
           State (TransfMState plaus agent evtid) (Prop.PFormula ModNumProp)
propify F = return Prop.F
propify T = return Prop.T
propify (Atom npAtom) =
  return $ Prop.Atom $ propifyAtom npAtom
propify (Not f) = Prop.Not <$> propify f
propify (And f1 f2) = do
  tf1 <- propify f1
  tf2 <- propify f2
  return (Prop.And tf1 tf2)
propify (Or f1 f2) = do
  tf1 <- propify f1
  tf2 <- propify f2
  return (Prop.Or tf1 tf2)
propify (Imp f1 f2) = do
  tf1 <- propify f1
  tf2 <- propify f2
  return (Prop.Imp tf1 tf2)
propify (Iff f1 f2) = do
  tf1 <- propify f1
  tf2 <- propify f2
  return (Prop.Iff tf1 tf2)
propify otherF = do
  (num, propMap) <- get
  let res = M.lookup otherF propMap
  let (newNum, newPropMap, mnp) = case res of
        Just oneMNP -> (num, propMap, oneMNP)
        Nothing -> (num+1, M.insert otherF newProp propMap, newProp)
          where
            newProp = MNP{mpname = modName ++ show num, mptype = Modal, midx = num}
  put (newNum, newPropMap)
  return (Prop.Atom mnp)

propifySet :: (Ord agent, Ord evtid, Ord plaus) =>
           Set (CDLFormulaP NumProp plaus agent evtid) ->
          (Set (Prop.PFormula ModNumProp), TransfMState plaus agent evtid)
propifySet = S.foldl' propifyChain (S.empty, (0, M.empty))
  where
    propifyChain (propifiedSet, st) fml = (S.insert val propifiedSet, newSt)
      where
        (val, newSt) = runState (propify fml) st

-- Remember to carry over the index integer for every propification
unpropify :: (Ord agent, Ord evtid) =>
             M.Map ModNumProp (CDLFormulaP NumProp plaus agent evtid) ->
             Prop.PFormula ModNumProp ->
             CDLFormulaP NumProp plaus agent evtid
unpropify revMap propFormula = let
  co f1 (Prop.:&:) f2 = And (unpropify revMap f1) (unpropify revMap f2)
  co f1 (Prop.:|:) f2 = Or (unpropify revMap f1) (unpropify revMap f2)
  co f1 (Prop.:=>:) f2 = Imp (unpropify revMap f1) (unpropify revMap f2)
  co f1 (Prop.:<=>:) f2 = Iff (unpropify revMap f1) (unpropify revMap f2)
  ne f1 = Not $ unpropify (revMap) f1
  bl True = T
  bl False = F
  at MNP{mpname = pn, mptype = Propos, midx = idxInt} =
    (Atom NP {pname = pn, idx = idxInt})
  at p@MNP{mpname = pn, mptype = Modal, midx = idxInt} =
    revMap M.! p 
  in
    Prop.foldPropositional co ne bl at propFormula

reverseMap :: (Ord k, Ord v) => 
              (Map k v) -> (Map v k)
reverseMap m = M.foldlWithKey' (\s k v -> M.insert v k s) M.empty m

-- ISSUE!!!! We may have something like:
-- "AND p (OR q (OR [alpha]r (Bel C s)))"
-- We replace all such formulas by fresh atoms during planning
-- and keep a map of such formulas

-- Parameterise simple BFS planning with:
-- others' beliefs about current status (and plausibility)
-- dynamic operators (and note that [act] does not include: plaus, obs)
-- ontologic statements (and own beliefs)

-- TODO Maybe remove plausibility with something else (Ord plaus => ???),
-- although this allows for probability based measures
-- TODO revise what an event model is; 
-- An event model is a set of 
data EventModelP atom plaus agent evtid = EventModelP {
    eventModelId :: evtid,
    -- Events can, as opposed to beliefs, hold separate equivalence classes
    -- FIXME make it per agent!!!!
    -- events :: agent -> [[(EventP atom plaus agent evtid, plaus)]]
    -- events :: agent -> Maybe [[(EventP atom plaus agent evtid, plaus)]]
    -- events :: Map agent [[(EventP atom plaus agent evtid, plaus)]]
    modelEvents :: [[(EventP atom plaus agent evtid, plaus)]]
  }
  deriving (Eq, Ord, Show)
type EventModel = EventModelP NumProp Integer String String
data LitEffect atom = SetL atom |
                      UnsetL atom |
                      FlipL atom |
                      ForgetL atom
  deriving (Eq, Ord, Show)

  {--
actToward :: agent ->
             BeliefBase atom plaus agent evtid ->
             EventModelP atom plaus agent evtid ->
             Goal atom plaus agent evtid
actToward agt beliefs act 
--}

-- EventP that can be usable by any of the methods
-- Maybe data families, so we can assure that plaus is an Ord instance
data EventP atom plaus agent evtid = EventP {
    eventId :: evtid,
    precond :: CDLFormulaP atom plaus agent evtid,
    -- Any real state (not just doxastic) that complies with the precond may
    -- apply the event
    postcond :: ([LitEffect atom], Set (agent, CDLFormulaP atom plaus agent evtid)),
    -- NOTE We'll use a list of effects instead of a set, because they are
    -- order-sensitive (eg a flip after a set, although it doesn't make much
    -- sense; maybe a 'Map atom LitEffect' ?????)
    -- FIXME This would need to specify *which* update/revision method
    -- the agent would use; maybe a real CDL sentence???
    -- FIXME Again, this assumes a fixed ANDed formula scheme; to deal with ORs,
    -- we'd specify alternate outcomes to the event; lots of different mechanisms!!!
    -- When we consolidate effects, we lose sight of what happened, which is not
    -- useful for planning (or is it?), but might be for explaining / induction
    -- FIXME Events may have alternatives, and then undistinguishable
    -- effects between alternatives. A known state effect cannot be
    -- specified as undistinguishable... How do we model that?
    -- FIXME What about MentalState?
    -- Classical postconditions are set/unset atoms (see xxx). But
    -- we might think about general beliefs changing in ourselves or
    -- other agents. Actually, we should explicitly take visibility into account
    -- in planning, so unobservable results are mere beliefs (and actually planned
    -- states, since we rely on the accuracy of our action expectations)
    -- In any case, we will assume postconds are certain and observable
    -- until we can reconcile belief and action uncertainties with a single model.
    -- TODO investigate model linkage via events with belief revision constructs
    -- plausLevel :: plaus,
    -- Induces a partial order relation
    -- access :: agent -> Set (EventP atom plaus agent evtid),
    -- Each agent may handle its own event model
    model :: EventModelP atom plaus agent evtid
    -- FIXME Actually the events do not need the model at all. Either
    -- the planning algorithm or the reasoning algorithms will require
    -- a separate model (this should be good for reuse)
    -- To avoid type clutter, we'll follow the next convention:
    -- The first list holds the event itself, so that
    -- [[evt2]] represents undistinguishable events, and
    -- [[], [evt2]] represents distinguishable events
    -- otherwise ([], [[]])
    -- FIXME ^ What did I just write???
    -- We don't need a per outcome plaus, since it does not
    -- make sense, generally; if we need to compare, we will compare specific
    -- outcomes
    -- events :: Map agent [(EventP atom plaus agent evtid, plaus)]
  }  
  deriving (Typeable)
type Event = EventP NumProp Integer String String

instance (Eq evtid) => Eq (EventP atom plaus agent evtid) where
  (==) e1 e2 = (==) (eventId e1) (eventId e2)
instance (Ord evtid) => Ord (EventP atom plaus agent evtid) where
  compare ev1 ev2 = compare (eventId ev1) (eventId ev2)
  
  
instance (Show agent, Show plaus, Show evtid, Show atom) => Show (EventP atom plaus agent evtid) where
  show EventP {eventId = ev, precond = prc, postcond = psc} =
      "EventP{eventId=" ++ show ev ++
            ",precond=" ++ show prc ++
            ",postcond=" ++ show psc ++
            "}"

instance (Ord agent, Ord plaus, Show agent, Show plaus, Show evtid, Ord evtid, HasFixity atom,
  IsAtom atom) =>
    HasFixity (CDLFormulaP atom plaus agent evtid) where
  precedence = precedenceCdl
  associativity = associativityCdl

simplify :: (Ord evtid, Ord agent, Eq atom) =>
            CDLFormulaP atom plaus agent evtid ->
            CDLFormulaP atom plaus agent evtid
simplify (Or F f) = simplify f
simplify (Or f F) = simplify f
simplify (Or f1 f2) = Or (simplify f1) (simplify f2)
simplify (And T f) = simplify f
simplify (And f T) = simplify f
simplify (And f1 f2) = And (simplify f1) (simplify f2)
simplify (Imp f1 f2) = Imp (simplify f1) (simplify f2)
-- TODO simplify ->F and others
simplify (Iff f1 f2) = Iff (simplify f1) (simplify f2)
-- TODO simplify <->F and others
simplify (Act mod evt f) = Act mod evt (simplify f)
simplify (Bel agt f) = Bel agt (simplify f)
simplify f = f

-- TODO We assume always the safe belief when checking actions. But it could be
-- that we need some less plausible condition to "make" the action executable in our mind.
-- How do we fix that? --> Check expandBase in BeliefRevision
checkPrecond :: (Ord evtid, Ord agent, Ord plaus,
                 Eq plaus, Eq atom) =>
                [CDLFormulaP atom plaus agent evtid] ->
                EventP atom plaus agent evtid ->
                Bool
checkPrecond state action = not $ tableauxAnd ((precond action):state)

-- FIXME We need some kind of constructive proof. Eg we may have evidence
-- for p and q, but not r; this does not mean that action a (precond p^r)
-- is executable

isConsistent :: (Ord evtid, Ord agent, Ord plaus,
                 Eq plaus, Eq atom, Eq agent) =>
                S.Set (CDLFormulaP atom plaus agent evtid) ->
                CDLFormulaP atom plaus agent evtid ->
                Bool
isConsistent formset formula = not (tableauxAnd (formula:(S.toList formset)))

areConsistent :: (Ord evtid, Ord agent, Ord plaus,
                  Eq plaus, Eq atom, Eq agent) =>
                 S.Set (CDLFormulaP atom plaus agent evtid) ->
                 [CDLFormulaP atom plaus agent evtid] ->
                 Bool
areConsistent formset formulas = not (tableauxAnd (formulas ++ (S.toList formset)))

isImplied :: (Ord evtid, Ord agent, Ord plaus, 
              Eq plaus, Eq atom, Eq agent) =>
           S.Set (CDLFormulaP atom plaus agent evtid) ->
           CDLFormulaP atom plaus agent evtid ->
           Bool
isImplied base formula = isProven
  where
    wLabel :: WorldLabel
    (Node (_, isProven) _, wLabel) = runState tab 0
    tab = tableauxStepTM [] [[terms]] (Node ([], False) [])
    terms = L.map (After 0 []) ((Not formula):(S.toList base))



data Utter atom plaus agent evtid = ToldB agent (CDLFormulaP atom plaus agent evtid)
  | ToldF agent (Fact atom plaus agent evtid)
  | Asked agent (CDLFormulaP atom plaus agent evtid)
  -- FIXME change [atom] to [var] to allow wh-questions
  -- | Whasked agent (CDLFormulaP atom plaus agent evtid) [atom]
  | Requested agent evtid

data Fact atom plaus agent evtid = Saw (Lit.LFormula atom)
-- FIXME Saying something is also an action, btw
  | Watched agent (EventP atom plaus agent evtid)
  | Heard agent (Utter atom plaus agent evtid)

-- A MentalState comprises observations, goals and
-- a prioritized belief base
data MentalState atom plaus agent evtid = MentalState {
  currentState :: Set (Lit.LFormula atom),
  -- Hard goals: work toward assertion of a CDLFormulaP
  goals :: Set (CDLFormulaP atom plaus agent evtid),
  belBase :: BeliefBase atom plaus agent evtid
}

-- The mental model is built during planning using
-- doxastic sentences from the belief base
type EpistemicState atom plaus agent evtid =
  Map (agent) (MentalState atom plaus agent evtid)

-- FIXME Add *evidence* to beliefs
-- A past fact? A person? An inference? An abduction?
-- Idea for plausibility: 1-7, as n/8
-- Either move towards 4 (favoring "indistinguishability")
-- Or move towards the extremes (favoring strong opinions)
type BeliefBase atom plaus agent evtid = [(S.Set (CDLFormulaP atom plaus agent evtid), plaus)]

-- FIXME Maybe a "tableau term"?
-- A structure with a Map agent BeliefFrame is not useful, because
-- we fall to a simple "p v Bbq" formula :(
--   bfHistory :: [(time, [Fact atom plaus agent evtid])],
-- Events are *not* immutable. There are some papers for event model
-- induction
-- TODO review this;
-- Ok, so what we really need are event models (and
-- how we build those is another issue). But we also need
-- some sentences that summarize high level info about
-- events, in case we don't have the specific models, or
-- we need to express a query: "if we walk into the bar,
-- will we walk out of it alive?"

modalPrec :: Num a => a
modalPrec = 6

-- foldPropositionalCdl ho co ne tf at fm
precedenceCdl :: (Ord agent, Ord plaus, Show plaus, Show evtid, Ord evtid, Show agent, IsAtom atom,
    HasFixity atom, HasFixity (CDLFormulaP atom plaus agent evtid)) =>
  CDLFormulaP atom plaus agent evtid -> Precedence
precedenceCdl = foldPropositionalCdl 
  ho 
  co 
  (\_ -> notPrec) 
  (\_ -> boolPrec) 
  (const Pretty.leafPrec)
    where
      ho (Act _ _ _) = modalPrec
      ho (Bel _ _) = modalPrec
      co _ (Prop.:&:) _ = andPrec
      co _ (Prop.:|:) _ = orPrec
      co _ (Prop.:=>:) _ = impPrec
      co _ (Prop.:<=>:) _ = iffPrec

associativityCdl :: (Ord plaus, Ord agent, IsAtom atom, Show agent, Show evtid, Ord evtid, Show plaus,
    HasFixity atom) =>
  CDLFormulaP atom plaus agent evtid -> Associativity
associativityCdl = foldPropositionalCdl ho co (const InfixA) (const InfixN) (const InfixL)
    where
      ho (Act _ _ _) = InfixN
      ho (Bel _ _) = InfixN
      co _ (Prop.:&:) _ = InfixA
      co _ (Prop.:|:) _ = InfixA
      co _ (Prop.:=>:) _ = InfixR
      co _ (Prop.:<=>:) _ = InfixA -- yes, InfixA: (a<->b)<->c == a<->(b<->c)

foldPropositionalCdl :: (Ord agent, Ord plaus, Ord evtid, 
                         Show agent, Show evtid, Show plaus,
                         IsAtom atom) =>
                        ((CDLFormulaP atom plaus agent evtid) -> r)                     -- ^ fold on some higher order formula
                     -> ((CDLFormulaP atom plaus agent evtid) -> Prop.BinOp -> (CDLFormulaP atom plaus agent evtid) -> r) -- ^ fold on a binary operation formula.  Functions
                                                             -- of this type can be constructed using 'binop'.
                     -> ((CDLFormulaP atom plaus agent evtid) -> r)                     -- ^ fold on a negated formula
                     -> (Bool -> r)                        -- ^ fold on a boolean formula
                     -> (AtomOf (CDLFormulaP atom plaus agent evtid) -> r)              -- ^ fold on an atomic formula
                     -> (CDLFormulaP atom plaus agent evtid) -> r

                     
foldPropositionalCdl ho co ne tf at fm =
  Lit.foldLiteral' (prop) ne tf at fm
    where
      prop fm = case fm of
        Imp p q -> co p (Prop.:=>:) q
        Iff p q -> co p (Prop.:<=>:) q
        And p q -> co p (Prop.:&:) q
        Or p q -> co p (Prop.:|:) q
        _ -> ho fm 


instance (Ord agent, Ord plaus, IsAtom atom, Show agent, Show evtid, Ord evtid, Show plaus) =>
    Pretty (CDLFormulaP atom plaus agent evtid) where
  pPrintPrec = prettyDelFormula Top

-- TODO Retro generalize later using IsDynamic and IsDoxastic
prettyDelFormula :: (Ord plaus, Ord agent, Show plaus, IsAtom atom, Show agent, Ord evtid, Show evtid) =>
  Side -> HPJC.PrettyLevel -> Rational -> (CDLFormulaP atom plaus agent evtid) -> Doc  
prettyDelFormula side l r fm =
    maybeParens (testParen side r (precedence fm) (associativity fm)) $ foldPropositionalCdl ho co ne tf at fm
    where
      co :: (Ord agent, Ord plaus, Show plaus, IsAtom atom, Show agent, Ord evtid, Show evtid) =>
        (CDLFormulaP atom plaus agent evtid) ->
        Prop.BinOp ->
        (CDLFormulaP atom plaus agent evtid) ->
        Doc
      co p (Prop.:&:) q = prettyDelFormula LHS l (precedence fm) p HPJC.<> text "∧" HPJC.<> prettyDelFormula RHS l (precedence fm) q
      co p (Prop.:|:) q = prettyDelFormula LHS l (precedence fm) p HPJC.<> text "∨" HPJC.<> prettyDelFormula RHS l (precedence fm) q
      co p (Prop.:=>:) q = prettyDelFormula LHS l (precedence fm) p HPJC.<> text "⇒" HPJC.<> prettyDelFormula RHS l (precedence fm) q
      co p (Prop.:<=>:) q = prettyDelFormula LHS l (precedence fm) p HPJC.<> text "⇔" HPJC.<> prettyDelFormula RHS l (precedence fm) q
      ne :: (Ord plaus, Ord agent, Show plaus, IsAtom atom, Show agent, Ord evtid, Show evtid) =>
        (CDLFormulaP atom plaus agent evtid) -> Doc
      ne p = text "¬" HPJC.<> prettyDelFormula Unary l (precedence fm) p
      tf x = prettyBool x
      at x = pPrintPrec l r x
      ho p@(Act mod evt fm) = text "[" HPJC.<> text (show evt) HPJC.<>
                              text "#" HPJC.<> text (show mod) HPJC.<>
                              text "]" HPJC.<> prettyDelFormula Unary l (precedence p) fm
      ho p@(Bel ag fm) = text "K_" HPJC.<> text (show ag) HPJC.<>
                         text "_{" HPJC.<> prettyDelFormula Unary l (precedence p) fm HPJC.<> text "}" 




-- START ISFORMULA INSTANCE

class Prop.IsPropositional formula => IsDynamic formula where
    -- | Implication.  @x .=>. y = ((.~.) x .|. y)@
    afterMod :: EventModelType formula -> EventType formula -> formula -> formula
    type EventType formula
    type EventModelType formula
    foldDynamic' :: (formula -> r)                     -- ^ fold on some other formula
                       -> (formula -> r)                     -- fold on a dynamic formula
                       -> formula -> r

instance (Ord agent, Ord plaus, Show agent, Formulas.IsAtom atom, Show evtid, Ord evtid, Show plaus) =>
    IsDynamic (CDLFormulaP atom plaus agent evtid) where
  type EventType (CDLFormulaP atom plaus agent evtid) = EventP atom plaus agent evtid
  type EventModelType (CDLFormulaP atom plaus agent evtid) = EventModelP atom plaus agent evtid
  afterMod = Act
  foldDynamic' otherFun dynFun f@(Act _ _ _) =
    dynFun f
  foldDynamic' otherFun dynFun f =
    otherFun f

class (Prop.IsPropositional formula) => IsDoxastic formula where
    -- | Implication.  @x .=>. y = ((.~.) x .|. y)@
    belMod :: AgentType formula -> formula -> formula
    type AgentType formula
    foldDoxastic' :: (formula -> r)                     -- ^ fold on some other formula
                       -> (formula -> r)                     -- fold on a doxastic formula
                       -> formula -> r

instance (Ord plaus, Ord agent, Show agent, Formulas.IsAtom atom, Show evtid, Ord evtid, Show plaus) =>
    IsDoxastic (CDLFormulaP atom plaus agent evtid) where
  type AgentType (CDLFormulaP atom plaus agent evtid) = agent
  belMod = Bel
  foldDoxastic' otherFun dynFun f@(Bel _ _) =
    dynFun f
  foldDoxastic' otherFun dynFun f =
    otherFun f

instance (Ord agent, Ord evtid, Ord plaus, Formulas.IsAtom atom, Show agent, Show evtid, Show plaus) =>
    Lit.IsLiteral (CDLFormulaP atom plaus agent evtid) where
  naiveNegate = Not
  foldNegation normal inverted (Not x) = Lit.foldNegation inverted normal x
  foldNegation normal _ x = normal x
  foldLiteral' ho ne tf at fm =
      case fm of
        T -> tf True
        F -> tf False
        Atom a -> at a
        Not l -> ne l
        _ -> ho fm


instance (Ord plaus, Ord agent, Show agent, Formulas.IsAtom atom, Ord evtid, Show evtid, Show plaus) =>
    Prop.IsPropositional (CDLFormulaP atom plaus agent evtid) where
  (.|.) = Or
  (.&.) = And
  (.=>.) = Imp
  (.<=>.) = Iff
  foldPropositional' = foldPropositionalCdl
  foldCombination other dj cj imp iff fm =
      case fm of
        Or a b -> a `dj` b
        And a b -> a `cj` b
        Imp a b -> a `imp` b
        Iff a b -> a `iff` b
        _ -> other fm

mapFst f (a,b) = (f a, b)

instance (Show agent, Show evtid, Show plaus,
          Ord agent, Ord plaus, Ord evtid,
          Formulas.IsAtom atom) =>
    IsFormula (CDLFormulaP atom plaus agent evtid) where
  type AtomOf (CDLFormulaP atom plaus agent evtid) = atom 
  true = T
  false = F
  asBool T = Just True
  asBool F = Just False
  asBool _ = Nothing
  atomic = Atom
  overatoms :: (atom -> r -> r) -> (CDLFormulaP atom plaus agent evtid) -> r -> r
  overatoms = cdlOveratoms
  -- ^ Formula analog of iterator 'foldr'.
  onatoms = cdlOnatoms
  -- ^ Apply a function to the atoms, otherwise keeping structure (new sig)

{--
fromBool :: (Show agent, Ord agent, Formulas.IsAtom atom, Ord evtid, Show evtid, Show plaus) =>
  Bool -> (CDLFormulaP atom plaus agent evtid)
fromBool True = true
fromBool False = false
--}

cdlOnatomsLit :: (Ord atom) =>
  (atom -> atom) ->
  (Lit.LFormula atom) ->
  (Lit.LFormula atom)
cdlOnatomsLit at2At Lit.F = Lit.F
cdlOnatomsLit at2At Lit.T = Lit.T
cdlOnatomsLit at2At (Lit.Atom atom) =
  Lit.Atom (at2At atom)
cdlOnatomsLit at2At (Lit.Not formula) =
  Lit.Not (cdlOnatomsLit at2At formula)
  
cdlOnatomsEff :: (Ord atom) =>
  (atom -> atom) ->
  (LitEffect atom) ->
  (LitEffect atom)
cdlOnatomsEff at2At (SetL at) = SetL (at2At at)
cdlOnatomsEff at2At (UnsetL at) = UnsetL (at2At at)
cdlOnatomsEff at2At (FlipL at) = FlipL (at2At at)
  
cdlOnatoms :: (Ord evtid, Ord atom, Ord agent, Ord plaus) =>
  (atom -> atom) ->
  (CDLFormulaP atom plaus agent evtid) ->
  (CDLFormulaP atom plaus agent evtid)
cdlOnatoms at2At F = F
cdlOnatoms at2At T = T
cdlOnatoms at2At (Atom atom) =
  Atom (at2At atom)
cdlOnatoms at2At (Not formula) =
  Not (cdlOnatoms at2At formula)
cdlOnatoms at2At (And f1 f2) =
  And (cdlOnatoms at2At f1) (cdlOnatoms at2At f2)
cdlOnatoms at2At (Or f1 f2) =
  Or (cdlOnatoms at2At f1) (cdlOnatoms at2At f2)
cdlOnatoms at2At (Imp f1 f2) =
  Imp (cdlOnatoms at2At f1) (cdlOnatoms at2At f2)
cdlOnatoms at2At (Iff f1 f2) =
  Iff (cdlOnatoms at2At f1) (cdlOnatoms at2At f2)
cdlOnatoms at2At (Act model event formula) =
  Act (newModel)
      (cdlOnatomsEvt at2At newModel event)
      (cdlOnatoms at2At formula)
    where
      newModel = cdlOnatomsEvtMod at2At model
cdlOnatoms at2At (Bel agent formula) =
  Bel agent (cdlOnatoms at2At formula)

cdlOnatomsEvtMod at2At EventModelP {
    eventModelId = evMId,
    modelEvents = evs} =
  newModel 
    where
      newModel = EventModelP {
        eventModelId = evMId,
        modelEvents = L.map (L.map (mapFst $ cdlOnatomsEvt at2At newModel)) evs}

{--    
  where
    mapWPlaus at2At (evt, plaus) = (cdlOnatomsEvt at2At evt, plaus)    
modelEvents :: [EventP atom plaus agent evtid]
events :: Map agent [[(EventP atom plaus agent evtid, plaus)]]
--}
lit2Prop :: Lit.LFormula atom -> Prop.PFormula atom
lit2Prop Lit.T = Prop.T
lit2Prop Lit.F = Prop.F
lit2Prop (Lit.Atom a) = Prop.Atom a
lit2Prop (Lit.Not l) = Prop.Not $ lit2Prop l  



cdlOnatomsEvt at2At newModel EventP {
    eventId = evtId,
    precond = prec, 
    postcond = postc
  } =   
  EventP {
    eventId = evtId,
    precond = cdlOnatoms at2At prec,
    postcond = (L.map (cdlOnatomsEff at2At) $ fst postc,
                S.map (\(x,y) -> (x, cdlOnatoms at2At y)) $ snd postc),
    model = newModel
  }

cdlOveratoms :: (atom -> r -> r) -> (CDLFormulaP atom plaus agent evtid) -> r -> r
cdlOveratoms red F val = val
cdlOveratoms red T val = val
cdlOveratoms red (Atom atom) val = red atom val
cdlOveratoms red (Not formula) val = cdlOveratoms red formula val 
cdlOveratoms red (And f1 f2) val = (cdlOveratoms red f1 . cdlOveratoms red f2) val
cdlOveratoms red (Or f1 f2) val = (cdlOveratoms red f1 . cdlOveratoms red f2) val
cdlOveratoms red (Imp f1 f2) val = (cdlOveratoms red f1 . cdlOveratoms red f2) val
cdlOveratoms red (Iff f1 f2) val = (cdlOveratoms red f1 . cdlOveratoms red f2) val
cdlOveratoms red (Act model event formula) val = cdlOveratoms red formula val
cdlOveratoms red (Bel agent formula) val = cdlOveratoms red formula val

-----------------------------------------
-- Literals for observation / knowledge logics
{--

data LObsFormula agent atom
  = F
  | T
  | Atom atom
  | Obs agent atom
  | Not (LFormula atom)
  deriving (Eq, Ord, Read, Show, Data, Typeable)

-- Other example primitives: At Int Int for spatial logics, for example

-- | The class of formulas that can be negated.  Literals are the
-- building blocks of the clause and implicative normal forms.  They
-- support negation and must include true and false elements.
class IsFormula lit => IsLiteral lit where
    -- | Negate a formula in a naive fashion, the operators below
    -- prevent double negation.
    naiveNegate :: lit -> lit
    -- | Test whether a lit is negated or normal
    foldNegation :: (lit -> r) -- ^ called for normal formulas
                 -> (lit -> r) -- ^ called for negated formulas
                 -> lit -> r
    -- | This is the internal fold for literals, 'foldLiteral' below should
    -- normally be used, but its argument must be an instance of 'JustLiteral'.
    foldLiteral' :: (lit -> r) -- ^ Called for higher order formulas (non-literal)
                 -> (lit -> r) -- ^ Called for negated formulas
                 -> (Bool -> r) -- ^ Called for true and false formulas
                 -> (AtomOf lit -> r) -- ^ Called for atomic formulas
                 -> lit -> r

instance IsAtom atom => IsFormula (LFormula atom) where
    type AtomOf (LFormula atom) = atom
    asBool T = Just True
    asBool F = Just False
    asBool _ = Nothing
    true = T
    false = F
    atomic = Atom
    overatoms = overatomsLiteral
    onatoms = onatomsLiteral

instance (IsFormula (LFormula atom), Eq atom, Ord atom) => IsLiteral (LFormula atom) where
    naiveNegate = Not
    foldNegation normal inverted (Not x) = foldNegation inverted normal x
    foldNegation normal _ x = normal x
    foldLiteral' _ ne tf at lit =
        case lit of
          F -> tf False
          T -> tf True
          Atom a -> at a
          Not f -> ne f

instance IsAtom atom => JustLiteral (LFormula atom)

instance IsAtom atom => HasFixity (LFormula atom) where
    precedence = precedenceLiteral
    associativity = associativityLiteral

instance IsAtom atom => Pretty (LFormula atom) where
    pPrintPrec = prettyLiteral

--}
-----------------------------------------

type WorldState plaus atom = (plaus, Set (Lit.LFormula atom))
-- type WorldState atom = 
type Action atom = forall plaus. WorldState plaus atom -> WorldState plaus atom
-- A more declarative action definition?

-- Note that the product takes a world equivalence model
-- and an event equivalence model to build new equivalences
-- W1 -> W2 ^ A1 -> A2 => W1xA1 -> W2xA2
type EventId = String

-- TODO Interesting idea: a "Set formula" might need a specialized function
-- 

-- Formulae (worlds) are also ordered by plausibility within each partition
class (IsFormula formula) => ProductUpdatable formula where
  product :: formula -> EventP (AtomOf formula) plaus agent evtid -> (formula, plaus)

  {--
altHst :: (Ord evtid, Ord agent) =>
  agent ->
  [(EventModelP atom plaus agent evtid, EventP atom plaus agent evtid)] ->
  [[(EventModelP atom plaus agent evtid, EventP atom plaus agent evtid)]]
-- altHst agt = expandL . (L.map (\x -> L.delete x (L.map (fst) ((concat . events . model) x) )) )
-- TODO Concat is wrong, since the updates are of two kinds: distinguished
-- and undistinguished. Let's fix this at some point. Maybe its not an issue
-- in [alpha] formulas?

altHst agt = expandL . (
  L.map (
          (L.map (fst)) . (M.! agt) . events
        )
  )
--}
-- events :: Map agent [[(EventP atom plaus agent evtid, plaus)]]
-- FIXME relax restrictions on plausibility
altHst :: forall atom evtid agent plaus. (Ord agent, Eq evtid) =>
          agent ->
          [EvtModP atom plaus agent evtid] ->
          [[(EvtModP atom plaus agent evtid, plaus)]]
--           [([EvtModP atom plaus agent evtid], plaus)]
-- altHst agt = (L.map (\(evs, plaus) -> (evs, combine plaus)) ) . step1
altHst agt = step2
  where
    -- TODO check how to handle probabilities in 
    combine = id
    step1 = (L.map unzip) . step2
    step2 = expandL . step3
    step3 = (L.map (\(md, ev) -> L.map (\(ev, pl) -> ((md, ev), pl)) $ L.filter ((== ev) . fst) (concat $ modelEvents md) ) )
-- TODO take out alternatives and remove in each step the action that has already been selected
-- modelEvents :: Map agent [[(EventP atom plaus agent evtid, plaus)]]

    

expandL :: [[a]] -> [[a]]
expandL [] = [[]]
expandL (list:restLists) = concat $ L.map ( ((flip L.map) expanded) . (:) ) list
  where
    expanded = expandL restLists

class HasNext a where
  next :: a -> a
  zero :: a
type WorldLabel = Int
instance HasNext Int where
  next wl = wl + 1
  zero = 0
-- TODO Note that we only need distinct identifiers, so we could use
-- some kind of structure along the terms; class???

-- CDLFormulaP formula plaus agent evtid
-- EventP atom agent evtid

type EvtModP atom plaus agent evtid = (EventModelP atom plaus agent evtid, EventP atom plaus agent evtid)

data TabTerm world atom plaus agent evtid
  = After world [EvtModP atom plaus agent evtid] (CDLFormulaP atom plaus agent evtid)
  | Ok world [EvtModP atom plaus agent evtid]
  | Nok world [EvtModP atom plaus agent evtid]
  | Access agent (world, world)
  | Bottom
  deriving (Eq, Ord, Show)

type Probability = Float
data TabNodeType evtid = ProofBr |
  ActionBr evtid |
  OutcomeBr Probability

data TabNode tabid world atom plaus agent evtid = TabNode {
  isClosed :: Bool,
  nodePrefix :: [tabid],
  nodeBrType :: TabNodeType evtid,
  nodeId :: tabid,
  lastTermId :: tabid,
  nodeTerms :: [TermRow tabid world atom plaus agent evtid]}

type TabTermId tabid = [tabid]
data TermRow tabid world atom plaus agent evtid = TermRow {
  termId :: Maybe tabid,
  termRow :: TabTerm world atom plaus agent evtid,
  derivedFrom :: [TabTermId tabid]
}

type TabTree tabid world atom plaus agent evtid = Tree (TabNode tabid world atom plaus agent evtid)

initRow :: TabTerm world atom plaus agent evtid ->
           TermRow Int world atom plaus agent evtid
initRow term = TermRow {
  termId = Just 0,
  termRow = term,
  derivedFrom = []
}

{--
isSameTerm :: TermRow tabid world atom plaus agent evtid ->
              TermRow tabid world atom plaus agent evtid ->
              Bool
isSameTerm t1 t2 = termRow t1 == termRow t2
--}

initTabNode :: (Ord evtid, Ord agent, Eq atom, Eq agent, Eq world, HasNext world, HasNext tabid) =>
               TabNode tabid world atom plaus agent evtid
initTabNode = TabNode {
  isClosed = False,
  nodePrefix = [],
  nodeBrType = ProofBr,
  nodeId = zero,
  lastTermId = zero,
  nodeTerms = []}

{--
-- FIXME WIP HANDLE LISTS OF AND/OR


tableaux2 :: (Ord evtid, Ord agent, Eq atom, Eq agent) =>
            CDLFormulaP atom plaus agent evtid ->
            Bool
tableaux2 formula = isProven
  where
    (Node (_, isProven) _, wLabel) = runState (
        tableauxStepTM2 []
          [[[initRow $ After (0 :: WorldLabel) [] formula]]]
          (Node initTabNode [])
        )
        zero

filtSingleTermList :: [TermRow tabid world atom plaus agent evtid] ->
                      [TermRow tabid world atom plaus agent evtid] ->
                      [TermRow tabid world atom plaus agent evtid]
filtSingleTermList stableTermList singleTermList = L.filter ((==) stableTermList) singleTermList

deriveUnaryM :: (Ord evtid, Ord agent, Eq atom, Eq agent, Eq world, HasNext world, HasNext tabid) =>
                TermRow tabid world atom plaus agent evtid ->
                State world [[TermRow tabid world atom plaus agent evtid]]
deriveUnaryM trow = fmap (fmap (fmap build)) $ unaryRuleM $ termRow trow 
  where
    build tr = TermRow {
      termId = Nothing,
      termRow = tr,
      derivedFrom = [termId trow]
    }

deriveBinaryM :: (Ord evtid, Ord agent, Eq atom, Eq agent, Eq world, HasNext world, HasNext tabid) =>
                 TermRow tabid world atom plaus agent evtid ->
                 TermRow tabid world atom plaus agent evtid ->
                 State world [[TermRow tabid world atom plaus agent evtid]]
deriveBinaryM trow1 trow2 = fmap (fmap (fmap build)) $
  binaryRuleM (termRow trow1) (termRow trow2) 
  where
    build tr = TermRow {
      termId = Nothing,
      termRow = tr,
      derivedFrom = [termId trow1, termId trow2]
    }

tableauxStepTM2 :: (Ord evtid, Ord agent, Eq atom, Eq agent, Eq world, HasNext world) =>
                   [TermRow tabid world atom plaus agent evtid] ->
                   [[[TermRow tabid world atom plaus agent evtid]]] ->
                   TabTree tabid world atom plaus agent evtid ->
                   State world (TabTree tabid world atom plaus agent evtid)
tableauxStepTM2 stableTermList [] n =
  return n
tableauxStepTM2 stableTermList ttt@(curTerm:restTerms) node =
  let
    closed = isClosed node,
    prefix = nodePrefix node,
    brType = nodeBrType node,
    ndid = nodeId node,
    terms = nodeTerms node
  in
  -- trace ("1- stableTermList : " ++ (show stableTermList) ++ "\n2- terms : " ++ (show ttt) ++ "\n3- node : " ++ (show nnn)) $
  case curTerm of
    [singleTermList] -> do
      let filtSingleTermList = L.filter (isSameTerm stableTermList) singleTermList
      let unaryNewTerms = L.map deriveUnaryM filtSingleTermList
      let binaryNewTerms = L.map (uncurry deriveBinaryM)
        (allCombo filtSingleTermList (terms ++ stableTermList))
      newExpsBeforeFilter <-
          groupExpansions $ unaryNewTerms ++ binaryNewTerms
      let newExps = L.filter (notNullOrEmpty) newExpsBeforeFilter
      let newClosed = closed || Bottom `L.elem` (L.map (termRow) filtSingleTermList)
      let newNode = Node (terms ++ filtSingleTermList, newIsClosed) children
      if newClosed then return newNode else 
        tableauxStepTM2 stableTermList (restTerms ++ newExps) newNode
    multipleTermListList -> do
      newChildren <- mapM (\tl -> tableauxStepTM
        (terms ++ stableTermList)
        (restTerms ++ [[tl]])
        (Node ([], False) [])) multipleTermListList
      let allClosed = isClosed || L.all (\(Node (_, c) _) -> c) newChildren
      return (Node (terms, allClosed) newChildren)
--}

-- TODO Do something more interesting than sorting the list
-- FIXME This was needed when event probs were combined
-- Right now we're not handling them, so we will just 
-- extract the event model
{--
sortHistories :: (Ord plaus) =>
                 [([EvtModP atom plaus agent evtid], plaus)] ->
                 [[EvtModP atom plaus agent evtid]]
sortHistories = L.map (fst) . L.sortOn (snd)
--}
sortHistories :: (Ord plaus) =>
                 [[(EvtModP atom plaus agent evtid, plaus)]] ->
                 [[EvtModP atom plaus agent evtid]]
sortHistories = L.map (L.map fst)


-- type CDLTabTerm = TabTerm WorldLabel String Int String String
-- TODO Explore the idea of separating terms by type beforehand
-- so we don't have to pass all term types through all functions

-- Monadic versions of the tableaux functions
-- NOTE
-- In the general case, one might need a Context: some immutable data on which
-- we depend to "run the rules", as well as a State monad through which
-- we thread our rule runs.
-- Events carry their own "correspondence" with them, so we don't need to add
-- a Context here, really.

-- type Tableaux = State WorldLabel

unaryRuleM :: (Ord plaus, Ord evtid, Ord agent, Eq atom, Eq agent, Eq world, HasNext world) =>
              TabTerm world atom plaus agent evtid ->
              State world [[TabTerm world atom plaus agent evtid]]
unaryRuleM (After w2 hst (And f1 f2)) =
  return [[After w2 hst f1, After w2 hst f2]]
-- FIXME change the not to a neg function
unaryRuleM (After w2 hst (Not (Not f1))) =
  return [[After w2 hst f1]]
unaryRuleM (After w2 hst (Or f1 f2)) =
  return [[After w2 hst f1], [After w2 hst f2]]
-- FIXME change the not to a neg function
unaryRuleM (After w2 hst (Imp f1 f2)) =
  return [[After w2 hst (Not f1)], [After w2 hst f2]]
unaryRuleM (After w2 hst (Iff f1 f2)) =
  return [[After w2 hst (Not f1), After w2 hst (Not f2)], [After w2 hst f1, After w2 hst f2]]
{--
-- FIXME New histories are created by combining all relation-accessible events
-- whose preconditions are fulfilled; maybe change only the *last* component?
unaryRuleM (After w [] (Not (Know ag f))) =
  return [[]]
--}
unaryRuleM (After w hst (Not (Bel ag f))) =
  (return . L.concat) <$> sequence (L.map (negBaRule ag w f) (sortHistories $ altHst ag hst ))
  -- NOTE!!! This is the return for the Monad instance of []
  -- We cannot use "[]" as a function
unaryRuleM (Nok w []) = return [[Bottom]]
-- Note that the following two rules are only valid because we reason only
-- about doxastic effects. Actions might have preconditions that would need to
-- be applied "running" their histories.
-- We cannot use a plain 'hst' variable since we need to rule out []
unaryRuleM (After w (h1:ht) (Atom p)) =
  return [[After w [] (Atom p)]]
unaryRuleM (After w (h1:ht) (Not (Atom p))) =
  return [[After w [] (Not (Atom p))]]
unaryRuleM (After w hst (Not (Act mod evt post))) =
  return [[Ok w ((mod,evt):hst), After w ((mod,evt):hst) (Not post)]]
unaryRuleM (After w hst (Act mod evt post)) =
  return [[Nok w ((mod,evt):hst)], [Ok w ((mod,evt):hst), After w ((mod,evt):hst) post]]
unaryRuleM (Ok w ((mod,evt):hst)) =
  return [[After w hst (precond evt), Ok w hst]]
unaryRuleM (Nok w ((mod,evt):hst)) =
  return [[After w hst (Not (precond evt)), Ok w hst], [Nok w hst]]
-- MISSING: two rules for event union
unaryRuleM _ =
  return [[]]
-- TODO Turn "Not f" into "neg f" and use a class of negatable formulae
-- as in ATP
negBaRule ag w f hst' = do
  cw <- get
  let nw = next cw
  put nw
  return [Access ag (w, nw), Ok nw hst', After nw hst' (Not f)]


binaryRuleM :: (HasNext world,
                Ord plaus, Ord evtid, Ord agent, 
                Eq plaus, Eq atom, Eq agent, Eq world) =>
               TabTerm world atom plaus agent evtid ->
               TabTerm world atom plaus agent evtid ->
               State world [[TabTerm world atom plaus agent evtid]]
-- TODO Should this be limited to Atom-ic propositions?
binaryRuleM (After w1 hst1 p1) (After w2 hst2 (Not p2))
  | (w1 == w2) && (hst1 == hst2) && (p1 == p2) = return [[Bottom]]
binaryRuleM (After w1 hst1 (Not p1)) (After w2 hst2 p2)
  | (w1 == w2) && (hst1 == hst2) && (p1 == p2) = return [[Bottom]]
  -- FIXME This requires using an event model (maybe taking it from the State
  -- monad?). Each event in the hst is replaced by an accessible event; other
  -- rules check the preconditions later on.
binaryRuleM (After w1 hst (Bel ag1 p)) (Access ag2 (w2, w3))
  | (ag1 == ag2) && (w1 == w2) = return $ baRule ag1 hst w3 p
binaryRuleM (Access ag2 (w2, w3)) (After w1 hst (Bel ag1 p))
  | (ag1 == ag2) && (w1 == w2) = return $ baRule ag1 hst w3 p
binaryRuleM (Ok w1 hst1) (Nok w2 hst2)
  | (w1 == w2) && (hst1 == hst2) = return [[Bottom]]
binaryRuleM (Nok w1 hst1) (Ok w2 hst2)
  | (w1 == w2) && (hst1 == hst2) = return [[Bottom]]
-- FIXME Currently one needs to manually add B(Atom) rules for existing
-- atoms
binaryRuleM _ _ = return [[]]
baRule ag hst aW aP = L.concat (L.map baRuleBranch (sortHistories $ altHst ag hst))
  where
    baRuleBranch aHst = [ [Ok aW aHst, After aW aHst aP], [Nok aW aHst]]

data TableauxBranch world atom plaus agent evtid = TableauxBranch {
    terms :: Set (TabTerm world atom plaus agent evtid),
    lastW :: world
  }
  deriving (Eq, Ord, Show)

emptyToMaybe TableauxBranch{terms = tbts}
  | S.null tbts = Nothing
emptyToMaybe tb = Just tb

fullCombo l1 l2 = [(i,j) | i <- l1, j <- l2]
selfCombo [] = []
selfCombo (x:xs) = L.map ((,) x) xs ++ selfCombo xs
allCombo curB prevB = selfCombo curB ++
                      fullCombo prevB curB

byLength la lb = compare (L.length la) (L.length lb)

type TermTree world atom plaus agent evtid = Tree ([TabTerm world atom plaus agent evtid], Bool)

-- TODO provide a resumable tableau where you can add
-- some more preconditions?
-- Returns True if all branches are closed
tableaux :: (Ord evtid, Ord agent, Ord plaus,
             Eq plaus, Eq atom, Eq agent) =>
            CDLFormulaP atom plaus agent evtid ->
            Bool
tableaux formula = isProven
  where
    (Node (_, isProven) _, wLabel) = runState (
        tableauxStepTM
        []
        [[[After (0 :: WorldLabel) [] formula]]]
        (Node ([], False) [])
      ) 0

-- FIXME There's a corner case: T derived from []
tableauxAnd :: (Ord evtid, Ord agent, Ord plaus,
                Eq plaus, Eq atom, Eq agent) =>
               [CDLFormulaP atom plaus agent evtid] ->
               Bool
tableauxAnd formulas = isProven
  where
    (Node (_, isProven) _, wLabel) = runState (
        tableauxStepTM
        []
        [[L.map (After (0 :: WorldLabel) [] ) formulas]]
        (Node ([], False) [])
      ) 0

-- FIXME Allow a *resumable* tableaux, so we can backtrack:
-- once a tableau is written, some more sentences are added
-- and crossed with the sentences already present there
-- Would help with the belief base algorithms
-- FIXME Allowing ANDed terms as an input to the tableau would be a
-- plus for normal forms
tableauxStepTM :: (Ord evtid, Ord agent, Ord plaus,
                   Eq plaus, Eq atom, Eq agent, Eq world,
                   HasNext world) =>
                  [TabTerm world atom plaus agent evtid] ->
                  [[[TabTerm world atom plaus agent evtid]]] ->
                  TermTree world atom plaus agent evtid ->
                  State world (TermTree world atom plaus agent evtid)
tableauxStepTM stableTermList [] n =
  return n
tableauxStepTM stableTermList ttt@(curTerm:restTerms) nnn@(Node (terms, isClosed) children) =
  -- trace ("1- stableTermList : " ++ (show stableTermList) ++ "\n2- terms : " ++ (show ttt) ++ "\n3- node : " ++ (show nnn)) $
  case curTerm of
    [singleTermList] -> do
      let filtSingleTermList = L.filter (`L.notElem` stableTermList) singleTermList
      newExpsBeforeFilter <-
          groupExpansions $
            L.map unaryRuleM filtSingleTermList ++
              L.map (uncurry binaryRuleM)
              (allCombo filtSingleTermList (terms ++ stableTermList))
      let newExps = L.filter (notNullOrEmpty) newExpsBeforeFilter
      let newIsClosed = isClosed || Bottom `L.elem` filtSingleTermList
      let newNode = Node (terms ++ filtSingleTermList, newIsClosed) children
      -- No need to expand more terms, since we already have a Bottom
      if newIsClosed then return newNode else 
        tableauxStepTM stableTermList (restTerms ++ newExps) newNode
    multipleTermListList -> do
      -- return $ error (show multipleTermListList)
      newChildren <- mapM (\tl -> tableauxStepTM
        (terms ++ stableTermList)
        (restTerms ++ [[tl]])
        (Node ([], False) [])) multipleTermListList
      let allClosed = isClosed || L.all (\(Node (_, c) _) -> c) newChildren
      return (Node (terms, allClosed) newChildren)

notNullOrEmpty :: [[a]] -> Bool
notNullOrEmpty [] = False
notNullOrEmpty [[]] = False
notNullOrEmpty _ = True

-- Takes a list of stateful expansions and groups them according
-- to length
groupExpansions :: [State world [[TabTerm world atom plaus agent evtid]]] ->
                   State world [[[TabTerm world atom plaus agent evtid]]]
groupExpansions expList =
  join <$> sortedExpList
    where
      join :: ([[a]], [[a]]) -> [[a]]
      -- IDEALLY
      -- a: group length-1 expansions into a single expansion
      -- b: group a number m of length-2 expansions into a 2^m expansion
      -- Really a good idea??? 
      -- Still doesn't work
      -- join ([], b) = b
      -- join (a, []) = a
      -- join (a, b) = ((concat a):b)
      join (a, b) = a ++ b
      sortedExpList = do
        exps <- sequence expList
        return $ L.partition ((== 1) . L.length) exps

groupMaybes :: Maybe [a] -> Maybe [a] -> Maybe [a]
groupMaybes Nothing Nothing = Nothing
groupMaybes Nothing (Just l) = Just l
groupMaybes (Just l) Nothing = Just l
groupMaybes (Just l1) (Just l2) = Just (l1 ++ l2)


-- implication and subsumption
-- separate all sets into 1) single sets, and 2) plural sets
-- for each plural set, check subsumption/implication against the 
-- single set
-- If result is single, add to single set
-- If result is not single, add to next plural set
-- Repeat in next plural set 
subsumptionStep :: (Lit.IsLiteral lit, Ord lit) =>
                    S.Set (S.Set lit) ->
                    S.Set (S.Set lit)
subsumptionStep layer = subsumptionStepRec layer S.empty
subsumptionStepRec remain subsumed
  | S.null remain = subsumed
  | otherwise     = subsumptionStepRec newRemain newSubsumed
    where
      Just (theLits, newRemain) = S.minView remain
      (litsAreSubsumed, rest) = S.partition (((==) $ Just False) . subsumes theLits) subsumed
      (litsSubsume, unrelated) = S.partition (((==) $ Just True) . subsumes theLits) rest
      newSubsumed = case S.null litsAreSubsumed of
        True  -> S.insert theLits unrelated 
        False -> subsumed

-- remove common literals: if several clauses share a literal, extract the
  -- literal and merge the rest of the clauses

-- Just True means cls1 subsumes cls2 (cls2 includes cls1)
-- Just False means cls2 subsumes cls1 (cls1 includes cls2)
subsumes :: (Lit.IsLiteral lit, Ord lit) =>
            S.Set lit ->
            S.Set lit ->
            Maybe Bool
subsumes cls1 cls2
  | S.size cls2 > S.size cls1 = decide $ subsumesRec cls1 cls2 S.empty S.empty
  | otherwise                 = not <$> (decide $ subsumesRec cls2 cls1 S.empty S.empty)
  where
    decide (onlyCls1, common, onlyCls2)
      | S.null onlyCls1 = Just True
      | S.null onlyCls2 = Just False
      | otherwise       = Nothing
subsumesRec cls1 cls2 onlyCls1 common
  | S.null cls1 = (onlyCls1, common, cls2)
  | otherwise   = subsumesRec newCls1 newCls2 newOnlyCls1 newCommon
    where
      Just (aLit, newCls1) = S.minView cls1
      (newCls2, newOnlyCls1, newCommon) =
        case S.member aLit cls2 of
          True  -> (S.delete aLit cls2, onlyCls1, S.insert aLit common)
          False -> (cls2, S.insert aLit onlyCls1, common)

-- remove negated
implyStep :: (Lit.IsLiteral lit, Ord lit) =>
              S.Set (S.Set lit) ->
              S.Set (S.Set lit)
-- FIXME we need a delete :: a -> Set a -> (Maybe a, Set a) operation
-- to keep track of which cnfs have been modified, so we don't need
-- to compare negated and rest again
implyStep cnf = case (negated == rest) of
  True  -> S.union singles negated
  False -> implyStep $ S.union singles negated
  where
    (singles, rest) = S.partition ((==) 1 . S.size) cnf
    negated = S.filter (not . S.null) $ S.foldr' (S.map . implyRule . unwrap) rest singles
    unwrap single = aLit where Just (aLit, _) = S.minView single

implyRule :: (Lit.IsLiteral lit, Ord lit) =>
              lit ->
              S.Set lit ->
              S.Set lit
implyRule lit cls = S.delete (Lit.negate lit) cls

cnfStateMerge :: (Prop.IsPropositional pf, Prop.JustPropositional pf, Ord pf, NumAtom (AtomOf pf)) =>
                 Set pf -> 
                 Set (Set (Lit.LFormula (AtomOf pf)))
cnfStateMerge = implyStep .
                subsumptionStep .
                (S.foldr' (S.union) S.empty) .
                S.map (defcnfs)

