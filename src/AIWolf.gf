abstract AbstractSyntaxHC = Common, Noun ** {
flags startcat = Sentence ;
cat
  Subject;
  Target;
  Role;
  Species;
  Verb;
  TalkNum;
fun
  Talk : Integer -> Integer -> TalkNum ;

  Villager : Role ;
  Seer : Role ;
  Medium : Role ;
  Bodyguard : Role ;
  Werewolf : Role ;
  Possessed : Role ;

  Human : Species ;
  Werewolf : Species ;

  Estimate : Verb ;
  ComingOut : Verb
  Divination : Verb ;
  Guard : Verb ;
  Vote : Verb ;
  Attack : Verb ;
  Divined : Verb ;
  Identified : Verb ;
  Guarded : Verb ;
  Voted : Verb ;
  Attacked : Verb ;
  Agree : Verb ;
  Disagree : Verb ;
  Over : Verb ;
  Skip : Verb ;
  
-- 2.1. Sentences that express knowledge or intent (2 types)
[subject] ESTIMATE [target] [role]
[subject] COMINGOUT[target] [role]: The [subject] states that the [target]’s role is [role]

-- 2.2. Sentences about actions of the Werewolf game (4 types)
[subject] DIVINATION [target]: The [subject] divines the [target]
[subject] GUARD [target]: The [subject] guards the [target]
[subject] VOTE [target]: The [subject] votes on the [target]
[subject] ATTACK [target]: The [subject] attacks the [target]

-- 2.3. Sentences about the result of past actions (5 types)
[subject] DIVINED [target][species]:
-- The [subject] used the seer’s action on living [target] and obtained the result [species]
[subject] IDENTIFIED [target][species]:
-- The [subject] used the medium’s action on dead [target] and obtained the result [species]
[subject] GUARDED [target]: The [subject] guarded the [target]
[subject] VOTED [target]: The [subject] voted on the [target]
[subject] ATTACKED [target]: The [subject] attacked the [target]
-- 2.4. Sentences that express agreement or disagreement (2 types)
[subject] AGREE [talk number]
[subject] DISAGREE [talk number]
-- 2.5. Sentences related to the flow of the conversation (2 types)
OVER: “I have nothing else to say” – implies agreement to terminate current day of conversation
SKIP: “I have nothing to say now” – implies desire to continue the current day of conversation