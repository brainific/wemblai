module StripsPlanner (
)
where

  Formulas.IsAtom atom

-- EFP 2.0

data PossFrameP atom agent = PossFrame {
  -- Need not be exhaustive
  val :: Map atom Bool,
  agViews :: Map agent (PossFrame atom agent)
}

type PossFrame = PossFrameP NumProp String

data Action atom agent
  = OnticAction (Set atom) (Set atom)
  | SenseAction 
  | AnncmAction 

{--
Questions:
* Are preconditions also belief formulae? Definitions are not too clear.
--}

{--
How are beliefs updated according to actions? And how does nondeterminism affect this?
OBLIVIOUS: agent does not know that the action has taken place
PARTIALLY_OBS: agent knows action took place, but does not know effects
FULLY_OBS: agent knows action took place, and knows its effects
STUDY HOW AN EVENT MODEL CORRESPONDS TO A POSSIBILITY UPDATE
AN INFO STATE CORRESPONDS TO AN EQUIVALENCE CLASS OF EPISTEMIC MODELS
STUDY HIERARCHICAL POSSIBILITIES WRT BELIEF REVISION
--}
 
{--
Types of action:

* Ontic:
** Fluents are updated normally
** Possibilities:
*** if agent is OBLIVIOUS the possibility map does not change
*** if agent is FULLY_OBS the possibility maps is the union of
    all *possible* applications of the action in the possibilities
    of the current situation
* Sensing:
** Fluents do not change
** Possibilities:
*** if agent is OBLIVIOUS the possibility map does not change
*** if agent is PARTIALLY_OBS the possibility maps is 
*** if agent is FULLY_OBS the possibility maps is the union of

--}

-- Organize frames in a graph
-- type Graph = Array Vertex [Vertex]