module CDLMind.Scratch (test01) where

import Control.Applicative
import WemblAI (
  CDLFormulaP(..),
  NumProp(..)
)

test01 :: IO ()
test01 = putStrLn $ show $ (,) <$> [1,2,3] <*> [4,5,6]
test02 :: IO ()
test02 = putStrLn $ show $ (,) <$> [1,2,3] <*> [4,5,6]

{--
data EventModelP atom plaus agent evtid = EventModelP {
    eventModelId :: evtid,
    events :: Set (Set (EventP atom plaus agent evtid))
    -- Because 
  }
  deriving (Eq, Ord, Show)
-- EventP that can be usable by any of the methods
-- Maybe data families, so we can assure that plaus is an Ord instance
data EventP atom plaus agent evtid = EventP {
    eventId :: evtid,
    precond :: CDLFormulaP atom plaus agent evtid,
    -- Any real state (not just doxastic) that complies with the precond may
    -- apply the event
    postcond :: CDLFormulaP atom plaus agent evtid -> CDLFormulaP atom plaus agent evtid,
    -- or
    -- postcond :: (Set atom, Set atom)
    -- Set atoms and unset atoms
    -- Any action that ends up in this event applies the postcondition
    -- Even with no ontic postcondition, two undistinguishable doxastic states
    -- can be linked after an action is played if their actions are also linked
    plausLevel :: plaus,
    access :: Set (EventP atom plaus agent evtid),
    -- Each agent may handle its own event model
    model :: EventModelP atom plaus agent evtid
  }  
  deriving (Typeable)

  type CDLFormula = CDLFormulaP NumProp Int String String
--}