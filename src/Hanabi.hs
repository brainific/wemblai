module Hanabi (
  rndSplit
, whichNum
, whichSuit
, buildDeck
, shuffleDeck
, suits
, emptyHints
, useNHints
, useSHints
, initGame
, playCard
, discard
, giveHint
, Suit(..)
, Card(..)
, Game(..)
) where

import Data.List as L
import Data.Map.Strict as M

import System.Random

data Suit = YellowS | WhiteS | GreenS | RedS | BlueS
  deriving (Eq, Show)

data Card = Card Suit Int
  deriving (Eq, Show)

suits = [YellowS, WhiteS, GreenS, RedS, BlueS]
cardNos = [(1, 3), (2, 2), (3, 2), (4, 2), (5, 1)]

nums = L.concat $ L.map (uncurry $ flip replicate) cardNos

buildDeck = [Card s n | s <- suits, n <- nums]

-- shuffleDeck :: (RandomGen g) => g -> [a] -> [a]
shuffleDeck gen orig = shuffleDeckRec gen orig []

shuffleDeckRec gen [] shuffled = shuffled
shuffleDeckRec gen orig shuffled =
  shuffleDeckRec nextgen (hd++tl) (mid:shuffled)
  where
    (nextgen, hd, (mid:tl)) = rndSplit gen orig

{--
shuffleDeck :: (RandomGen g) => Rand g Int -> [a] -> [a]
shuffleDeck rnd orig = 
  --}

rndSplit :: (RandomGen g) => g -> [a] -> (g, [a], [a])
rndSplit oldGen orig = (newGen, hd, tl)
  where
    (link, newGen) = randomR (0, (length orig) - 1) oldGen
    (hd, tl) = L.splitAt link orig

data Table = Map Suit Int

type VisHand = [Card]

type HintHand = [(Maybe Suit, Maybe Int)]

type Hint a = (a, [Int])

emptyHints = L.replicate 5 (Nothing, Nothing)

whichNum :: VisHand -> Int -> [Int]
whichNum hand num = findIndices (\(Card s n) -> (n == num)) hand

whichSuit :: VisHand -> Suit -> [Int]
whichSuit hand suit = findIndices (\(Card s n) -> (s == suit)) hand

swap :: Int -> a -> [a] -> Maybe (a, [a])
swap pos elem lst = if pos >= length lst
  then Nothing
  else Just (old, pfx++(elem:sfx))
  where
    (pfx, (old:sfx)) = L.splitAt pos lst

useNHints :: [Hint Int] -> [(Maybe Suit, Maybe Int)] -> [(Maybe Suit, Maybe Int)]
useNHints hints oldhs = L.foldl' (replace) oldhs hints
  where
    replace oldhs (n, idxs) = L.foldl' (swap n) oldhs idxs
    swap n hs idx = prehand ++ ((oldsuit, Just n):posthand)
      where
        (prehand, ((oldsuit, _):posthand)) = L.splitAt idx hs

useSHints :: [Hint Suit] -> [(Maybe Suit, Maybe Int)] -> [(Maybe Suit, Maybe Int)]
useSHints hints oldhs = L.foldl' (replace) oldhs hints
  where
    replace oldhs (s, idxs) = L.foldl' (swap s) oldhs idxs
    swap s hs idx = prehand ++ ((Just s, oldint):posthand)
      where
        (prehand, ((_, oldint):posthand)) = L.splitAt idx hs

data Game = Game {
  hands :: [VisHand],
  hints :: [HintHand],
  table :: VisHand,
  deck :: VisHand,
  discardHeap :: VisHand,
  remainingHints :: Int,
  fails :: Int
}
  deriving (Eq, Show)

initGame :: Int -> Game
initGame seed = Game {
  hands = [h1, h2],
  hints = [noHints, noHints],
  table = [Card s 0 | s <- suits],
  deck = shuffledFinal,
  discardHeap = [],
  remainingHints = 7,
  fails = 0}
  where
    noHints = L.replicate 5 (Nothing, Nothing) 
    shuffled1 = shuffleDeck gen buildDeck
    (h1, shuffled2) = L.splitAt 5 shuffled1
    (h2, shuffledFinal) = L.splitAt 5 shuffled2
    gen = mkStdGen seed

type Move = Game -> Maybe Game

playCard :: Int -> Int -> Move
playCard player n gamea = case L.null decka of
  True  -> Nothing
  False -> Just gameb
  where
    tablea = table gamea
    handsa = hands gamea
    discarda = discardHeap gamea
    failsa = fails gamea
    decka@(fstDecka:deckb) = deck gamea
    Just (playera, handsb) = swap player playerb handsa
    Just (playedCard, playerb) = swap n fstDecka playera
    playableAt = L.findIndex id (L.map (isNext playedCard) tablea)
    isNext (Card plSuit plNum) (Card nxSuit nxNum)
      | nxSuit == plSuit && nxNum+1 == plNum = True
      | otherwise = False
    gameb = case playableAt of
      Just at -> gamea {
        hands = handsb,
        table = tableb,
        deck = deckb,
        fails = failsa}
          where
            Just (_, tableb) = swap at playedCard tablea
      Nothing -> gamea {
        hands = handsb,
        deck = deckb,
        discardHeap = (playedCard:discarda),
        fails = failsa+1}

discard :: Int -> Int -> Move
discard player n gamea = case L.null decka of
  True  -> Nothing
  False -> Just gameb
  where
    tablea = table gamea
    handsa = hands gamea
    discarda = discardHeap gamea
    failsa = fails gamea
    decka@(fstDecka:deckb) = deck gamea
    Just (playera, handsb) = swap player playerb handsa
    Just (playedCard, playerb) = swap n fstDecka playera
    gameb = gamea {
        hands = handsb,
        deck = deckb,
        discardHeap = (playedCard:discarda)}

giveHint :: Either Suit Int -> Int -> Move
giveHint hint player gamea = case remainingHints gamea > 0 of
  False -> Nothing
  True  -> Just gameb
  where
    playera = (hands gamea) !! player
    hintsa = hints gamea
    Just (hintsPlayera, hintsb) = swap player hintsPlayerb hintsa
    hintsPlayerb = case hint of
      Left st  -> useSHints [(st, whichSuit playera st)] hintsPlayera
      Right nm -> useNHints [(nm, whichNum playera nm)] hintsPlayera
    gameb = gamea {
      hints = hintsb,
      remainingHints = remainingHints gamea - 1}
          
data FinalState = Fail | Success
type GameStatus = Maybe FinalState

