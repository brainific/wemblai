module Moves (
  Move(..)
) where

import Data.Set as S
import Data.List as L

-- Actions are declared in a turn and resolved at the end of a turn

data Move var atom actname plaus agent =
  Say agent (Formula var atom actname plaus agent) (Set agent) |
  Ask agent (Question var atom actname plaus agent) (Set agent) |
  Request agent (Move var atom actname plaus agent) (Set agent) |
  -- Epistemic actions aim at reducing uncertainty in planning
  Does agent actname |
  FindGoal agent agent
  -- Don't understand?

data ActionSpec agent atom actname plaus = ActionSpec actname (S.Set (Result agent atom, plaus))

-- FIXME free vars should be tied to either quantifiers or questions?
-- A question without question vars means we don't know the truth
-- of the formula
data Question var atom actname plaus agent = Question (Set var) (Formula var atom actname plaus agent)

data Result agent atom = 
  Result (S.Set (OnticEffect atom)) (S.Set (EpistemicEffect agent atom))

data StateItem agent atom =
  State atom |
  Acting agent

data OnticEffect atom = 
  SetEff atom |
  UnsetEff atom

data EpistemicEffect agent atom = 
  Senses (StateItem agent atom) |
  Hidden (StateItem agent atom) 
  -- Visibility atoms as in: https://hal.archives-ouvertes.fr/hal-01592020/document
  -- Kp iff agent can see p, and p
  -- visibility is akin to "knowing whether" (Kp or K¬p)
  -- An effect may result in that an agent 

type Timestamp = Integer

-- A non-TS modified formula is implicitly "now"
data Formula var atom actname plaus agent
  = F
  | T
  | Until (Formula var atom actname plaus agent) (Formula var atom actname plaus agent)
  | Past Timestamp (Formula var atom actname plaus agent)
  | Atom atom
  | Not (Formula var atom actname plaus agent)
  | And (Formula var atom actname plaus agent)
        (Formula var atom actname plaus agent)
  | Or (Formula var atom actname plaus agent)
       (Formula var atom actname plaus agent)
  | Imp (Formula var atom actname plaus agent)
        (Formula var atom actname plaus agent)
  | Iff (Formula var atom actname plaus agent)
        (Formula var atom actname plaus agent)
  | Act (ActionSpec agent atom actname plaus)
  -- Communicating action results (only total models)
  | Done (Move var atom actname plaus agent)
  | Wants agent (Formula var atom actname plaus agent) plaus
  | Bel agent (Formula var atom actname plaus agent)

-- An agent has goals and beliefs
{--
data EpiEffect agent atom =  Observe agent atom |
  -- This translates into inserting into the 0-plaus layer
  Say agent (CDLFormulaP atom plaus agent evtid) |
  -- TODO include other performatives:
  Ask agent (CDLFormulaP atom plaus agent evtid) |
  -- Performatives regarding action negotiation
  Request agent (CDLFormulaP atom plaus agent evtid) evtid |
  Cancel agent evtid |
  Commit evtid | -- Propose if talking about self?
  Refuse evtid |
  Succeed agent evtid |
  Failed agent evtid |
  What (EpiEffect agent atom) |
  Tell agent (EpiEffect agent atom)
  Cfp (S.Set agent)
    deriving (Eq, Ord, Show)

-- We allow ontic and epistemic goals
data Goal atom plaus agent evtid = LitGoal (LFormula atom) |
  EpiGoal agent (CDLFormulaP atom plaus agent evtid)
  deriving (Eq, Ord, Show)


--}