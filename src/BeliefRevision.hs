{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ApplicativeDo #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE InstanceSigs #-}

module BeliefRevision (
  minBelSet,
  purify,
  makeConsistent,
  limitSoS,
  getSoS,
  expandsAndsSet,
  modNumAt,
  reverseMap,
  negSAnd,
  negateConj
  ) where

-- import
import WemblAI as WAI
import Data.Tree as T
import Data.Set as S
import Data.Map as M
import Data.List as L
import Data.Maybe as Mb
import qualified Data.Logic.ATP.Lit as Lit (
  LFormula(..)
 ,IsLiteral(..)
 ,negate
 ,(¬) )
import qualified Data.Logic.ATP.Prop as Prop (
  -- Prop(P, pname)
  PFormula(..)
 ,IsPropositional(..)
 ,JustPropositional(..)
 ,simpdnf
 ,simpcnf
 ,(∨)
 ,(∧)
 ,(.<=>.)
 ,(.&.)
 ,(.|.)
 ,BinOp(..) )
import Data.Logic.ATP.Formulas as Formulas (
  fromBool
 ,AtomOf(..)
 ,IsAtom
 ,prettyBool )
import Data.Logic.ATP.DefCNF as DefCNF (
  defcnfs
 ,NumAtom(..) )
import Data.Logic.ATP.DP as DP (
      dp,   dpsat,   dptaut
    , dpli, dplisat, dplitaut
    , dpll, {--dpllsat,--} dplltaut
    , dplb, dplbsat, dplbtaut
    , testDP
    )
import Control.Monad.State.Strict

-- Generates a list of sets of propositions that are compatible,
-- as fallback sets
expandBase :: (Ord evtid, Ord agent, Ord plaus,
               Eq atom, Eq agent, Eq plaus) =>
              BeliefBase atom plaus agent evtid ->
              [(Set (CDLFormulaP atom plaus agent evtid), [CDLFormulaP atom plaus agent evtid])]
expandBase base = expandStep base [] []
  where
    expandStep [] _ acc = acc
    expandStep ((b, _):rest) combined acc = let
        nextCombined = (S.toList b) ++ combined
      in case WAI.tableauxAnd nextCombined of
        True ->
          acc
        False ->
          expandStep rest nextCombined (acc ++ [(b, combined)])

modNumAt :: NumProp -> ModNumProp
modNumAt NP{pname = pn, idx = i} =
  MNP{mpname = pn, mptype = WAI.Propos, midx = i}

negateConj ::
  S.Set (CDLFormulaP atom plaus agent evtid) ->
  S.Set (CDLFormulaP atom plaus agent evtid)
negateConj logicSet = let
    asList = S.toList logicSet
    -- TODO check if lazy matches are needed
    (oneSent:rest) = asList
  in
    case (S.size logicSet) of
    0 -> logicSet
    1 -> S.singleton $ WAI.Not oneSent
    2 -> S.singleton $ L.foldl' (WAI.Or) oneSent rest

negSAnd :: (IsAtom atom, Ord agent, Ord evtid,
            Plaus plaus, Show agent, Show evtid, Show plaus) => 
           Set (CDLFormulaP atom plaus agent evtid) ->
           CDLFormulaP atom plaus agent evtid
negSAnd = Lit.negate . (L.foldl1' And) . S.toList

-- FIXME Maybe in planning we need even negating the safest layer
-- There is actually no assurance that the base layer will be safe
getSoS :: (Ord evtid, Ord agent, Ord plaus, Plaus plaus,
           Eq atom, Eq agent, Eq plaus, IsAtom atom,
           Show plaus, Show evtid, Show agent) =>
          BeliefBase atom plaus agent evtid ->
          BeliefBase atom plaus agent evtid
getSoS [] = []
getSoS hbase@[_] = hbase
getSoS ((hbase1,p1):(hbase2,p2):htail) = (neg:rest)
  where
    neg = (S.insert (negSAnd hbase2) hbase1, (plausToProb p1) - (plausToProb p2))
    rest = getSoS ((S.union hbase1 hbase2, p2):htail)

{--
getSoS_depr :: (Ord evtid, Ord agent, Ord plaus,
           Eq atom, Eq agent, Eq plaus, IsAtom atom,
           Show plaus, Show evtid, Show agent) =>
          BeliefBase atom plaus agent evtid ->
          [BeliefBase atom plaus agent evtid]
getSoS_depr base =
  where
    ((know, []):layers) = expandBase base
    negSAnd (fstLy, restLy) = ((Lit.negate . (foldl1' WAI.And) . S.toList) fstLy):restLy
    beliefs = case layers of
      [] -> [[]]
      ly -> (L.map (negSAnd) ly) ++ [unroll $ last ly]
    unroll (setFml, listFml) = (S.toList setFml) ++ listFml
--}

limitSoS :: (Ord evtid, Ord agent, Ord plaus,
             Eq plaus, Eq atom, Eq agent, IsAtom atom,
            Show plaus, Show evtid, Show agent) =>
            [CDLFormulaP atom plaus agent evtid] ->
            BeliefBase atom plaus agent evtid ->
            BeliefBase atom plaus agent evtid
--  type BeliefBase atom plaus agent evtid = [(S.Set (CDLFormulaP atom plaus agent evtid), plaus)]
limitSoS cond base = (purify  ) (limitSosRec revBase Nothing)
-- limitSoS cond base = (purify . Mb.fromJust . foldRadExp) (limitSosRec revBase Nothing)
  where
    -- foldRadExp b = L.foldl' (flip radicalExpand) b cond
    revBase = L.reverse base
    limitSosRec revBase@(mostPr:restPr) Nothing = case (not $ tableauxAnd (cond ++ catBase revBase)) of
      True  -> revBase
      False -> limitSosRec restPr (Just mostPr)
    limitSosRec revBase@(mostPr:restPr) (Just discard) = case (not $ tableauxAnd (cond ++ catBase revBase)) of
      True  -> addToMost (fst discard) revBase
      False -> limitSosRec restPr (Just mostPr)
    addToMost fmlSet ((mostFmlSet, mostProb):rest) =
      ((S.insert (Lit.negate $ catSWithAnd fmlSet) mostFmlSet, mostProb):rest)
    catSWithAnd = S.foldl' (WAI.And) (WAI.T)
    catBase = L.concat . (L.map (S.toList . fst))


{--
TODO redo previous with foldM

import Control.Monad(foldM)

sumSome :: (Num n,Ord n) => n -> [n] -> Either n n
sumSome thresh = foldM f 0
  where
    f a n 
      | a >= thresh = Left a
      | otherwise   = Right (a+n)
--}

conservativeExpand :: (WAI.Plaus plaus,
                       Ord plaus, Ord evtid, Ord agent,
                       Eq atom, Eq agent, Eq plaus) =>
                      CDLFormulaP atom plaus agent evtid ->
                      BeliefBase atom plaus agent evtid ->
                      Maybe (BeliefBase atom plaus agent evtid)
conservativeExpand belief base@((safe,safeP):rest) = case isConsistent safe belief of
  False -> Nothing
  True  -> Just (base ++ [(S.singleton belief, WAI.minP)])

radicalExpand :: (WAI.Plaus plaus,
                  Ord plaus, Ord evtid, Ord agent, Ord atom,
                  Eq atom, Eq agent, Eq plaus) =>
                  CDLFormulaP atom plaus agent evtid ->
                  BeliefBase atom plaus agent evtid ->
                  Maybe (BeliefBase atom plaus agent evtid)
radicalExpand belief base@((safe,safeP):rest) = case isConsistent safe belief of
  False -> Nothing
  True  -> Just ((S.insert belief safe,safeP):rest)
                  
purify :: (Ord plaus, Ord atom, Ord evtid, Ord agent,
           Eq plaus, Eq atom, Eq agent) =>
          BeliefBase atom plaus agent evtid ->
          BeliefBase atom plaus agent evtid
purify base = L.filter (not . S.null . fst) $ purifyStep base S.empty []
  where
    purifyStep ((layer,p):rest) accRules newBase =
      purifyStep rest newAccRules newNewBase
      where
        nonImplied = S.filter (not . isImplied accRules) layer
        newAccRules = S.union accRules nonImplied
        newNewBase = case (S.null nonImplied) of
          True  -> newBase
          False -> newBase ++ [(nonImplied,p)]
    purifyStep [] _ newBase = newBase

makeConsistent :: (Ord atom, Ord evtid, Ord agent, Ord plaus,
                   Eq plaus, Eq atom, Eq agent) =>
                  BeliefBase atom plaus agent evtid ->
                  BeliefBase atom plaus agent evtid
makeConsistent base = L.filter (not . S.null . fst) $ makeConsStep base S.empty []
  where
    makeConsStep ((layer,p):rest) accRules newBase =
      makeConsStep rest newAccRules newNewBase
      where
        consistent = S.filter (isConsistent accRules) layer
        newAccRules = S.union accRules consistent
        newNewBase = case (S.null consistent) of
          True  -> newBase
          False -> newBase ++ [(consistent,p)]
    makeConsStep [] _ newBase = newBase

expandsAndsSet :: (Ord agent, Ord evtid, Ord atom, Ord plaus) =>
                  Set (CDLFormulaP atom plaus agent evtid) ->
                  Set (CDLFormulaP atom plaus agent evtid)
expandsAndsSet = S.fromList . expandAnds . S.toList
expandAnds ((And f1 f2):restfs) = expandAnds (restfs ++ [f1, f2])
expandAnds (other:restfs) = other:(expandAnds restfs)
expandAnds [] = []

minBelSet :: (Ord evtid, Ord atom, Ord agent, Ord plaus,
              Eq plaus, Eq atom, Eq agent) =>
             BeliefBase atom plaus agent evtid ->
             S.Set (CDLFormulaP atom plaus agent evtid)
minBelSet belBase = minBelSetRec belBase S.empty
  where
    minBelSetRec [] accBelSet = accBelSet
    minBelSetRec ((layer,_):belBase) accBelSet = case S.null (S.filter (isConsistent accBelSet) layer) of
      False ->
        minBelSetRec belBase (S.union accBelSet layer)
      True ->
        accBelSet

{--
plaus 0: knowledge
plaus 1-7: belief, from 93.75 to 56.25
probabilities: 1-15, from 6.25 to 93.75
0 and 16 mean 0 and 100 probability
the sum of all events in an event model must total 16
data EventModelP atom poss agent evtid = EventModelP {
  eventModelId :: evtid,
  modelEvents :: Map agent [[(EventP atom plaus agent evtid, plaus)]]
}
data EventP atom poss agent evtid = EventP {
    eventId :: evtid,
    precond :: CDLFormulaP atom plaus agent evtid,
    postcond :: ([LitEffect atom], Set (agent, CDLFormulaP atom plaus agent evtid))
}
type BeliefBase atom plaus agent evtid = [(S.Set (CDLFormulaP atom plaus agent evtid), plaus)]
--}

-- Effects of the indistinguishable effects
-- Looks like a plaus base, but it's not that exactly
-- Knowledge is plaus 0, beliefs are plaus 1-7

-- precond i -> postcond i
-- preconds an happen at various levels in plaus
-- precond must be knowledge?
-- 0..i -> postcond 1
-- 0..j -> postcond 2
-- 0..k -> postcond 3
{--
b iff c'
a -> a',b'; end state: a',b'
a' -> b,c ; end state: a',b,c
--}
-- Get most common plaus level (at least knowledge)
-- how to reconcile? if only 1 outcome, it goes as knowledge
-- but if more than one outcome is present, then 

{--
expandOr :: (Show plaus, Show agent, Ord plaus, Ord agent, Show evtid, Ord evtid) =>
            agent ->
            MentalState NumProp plaus agent evtid ->
            [EventModelP NumProp plaus agent evtid] ->
            Map evtid (AndNode NumProp plaus agent evtid time)
expandOr myname state eventMList = M.map (buildAndNode state) actionMap
  where
    actionMap = M.fromList $ L.map (\ev -> (eventModelId ev, modelEvents ev)) eventMList
    buildAndNode state evtAlts = AndNode{alts = alterns}
      where
        alterns = L.map ((buildOrNode state) . (M.! myname) . events . fst) evtAlts
    buildOrNode state actions = OrNode{orBelBase = apply state possibleEffects}
      where
        apply state possibleEffects = redoPlaus $ revise $ effectStates ++ state
          where
            effectStates = L.map (WAI.mapFst (S.singleton . WAI.simplify)) possibleEffects
        redoPlaus = id -- FIXME rework plausibilities according to order?
        revise (new:old) = new:(L.map (mapFst $ S.filter (WAI.isConsistent (fst new))) old)
        catSWithAnd = S.foldl' (WAI.And) (WAI.T)
        catLWithOr = L.foldl' (WAI.Or) (WAI.F)
        consolidateInEvt :: _
        consolidateInEvt fs = (catLWithOr formulas, pl)
          where
            (formulas, pl:_) = unzip fs
        possibleEffects = 
          ((L.map (consolidateInEvt . L.map (WAI.mapFst (catSWithAnd . (buildEffect state))))) . _)
        groupByPlaus = L.groupBy (\x y -> snd x == snd y)

postState state (evt, plE) = case WAI.checkPrecond (S.toList $ maxBelSupport state) evt of
  True  -> Just (buildEffect evt state, plE)
  False -> Nothing
--}




-- Revision algorithms

-- Radical revision
-- Natural revision
-- Lexicographic revision